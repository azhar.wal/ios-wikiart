//
//  WAPaintingTitleTableViewSectionFooterView.swift
//  WikiArt2.0
//
//  Created by Waleed Azhar on 2017-10-09.
//  Copyright © 2017 Waleed Azhar. All rights reserved.
//

import UIKit

class WAPaintingTitleTableViewSectionFooterView: UITableViewHeaderFooterView {

    let paintingNameLabel = UILabel()
    let artistNameLabel = UILabel()
    let stackView = UIStackView()
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        let view = UIView()
        view.backgroundColor = .white
        backgroundView = view
        self.clipsToBounds = true

        stackView.translatesAutoresizingMaskIntoConstraints = false
        paintingNameLabel.translatesAutoresizingMaskIntoConstraints = false
        artistNameLabel.translatesAutoresizingMaskIntoConstraints = false
        
        stackView.axis = .vertical
        stackView.distribution = .fill
        stackView.alignment = .leading
        stackView.addArrangedSubview(paintingNameLabel)
        stackView.addArrangedSubview(artistNameLabel)
        
        contentView.addSubview(stackView)
        contentView.pinToEdges(view: stackView, excludingEdge: .Left)
        stackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 8.0).isActive = true
        
        paintingNameLabel.font = UIFont.systemFont(ofSize: 12, weight: .medium)
        paintingNameLabel.textColor = .black
        artistNameLabel.font = UIFont.systemFont(ofSize: 10, weight: .regular)
        artistNameLabel.textColor = .darkGray
        artistNameLabel.lineBreakMode = .byTruncatingMiddle
    }

    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureFor(painting:WAPainting) {
        paintingNameLabel.text = painting.titled
        artistNameLabel.text = painting.artistNamed
    }

}
