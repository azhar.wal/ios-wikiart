//
//  UIPaintingSearchResultTableViewCell.swift
//  WikiArt2.0
//
//  Created by Waleed Azhar on 2017-10-09.
//  Copyright © 2017 Waleed Azhar. All rights reserved.
//

import UIKit

class WAPaintingSearchResultTableViewCell: UITableViewCell {
    
    var paintingImageView = UIImageView()
    var first = true
    
    override func layoutSubviews() {
        paintingImageView.frame = contentView.bounds.insetBy(dx: 8, dy: 0)
        if first {
            first = false
            contentView.addSubview(paintingImageView)
            paintingImageView.contentMode = .scaleToFill
            paintingImageView.backgroundColor = .clear
            clipsToBounds = true
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        renderCellSelection(selected: isSelected)
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        renderCellSelection(selected: highlighted)
    }
    
    func renderCellSelection(selected: Bool) {
        let selectedAlpha: CGFloat = 0.8
        let normalAlpha: CGFloat = 1
        
        UIView.springAnimation(duration: 0.2, animation: {
            self.paintingImageView.alpha = selected ? selectedAlpha : normalAlpha
        }, completion: nil)
    }
    
    override func prepareForReuse() {
        paintingImageView.image = nil
        paintingImageView.frame = self.contentView.bounds
    }
    
    func configure(forPainting painting: WAPainting) {
        paintingImageView.loadPainting(painting: painting, size: .original)
    }
}
