//
//  WASearchViewController.swift
//  WikiArt2.0
//
//  Created by Waleed Azhar on 2017-10-09.
//  Copyright © 2017 Waleed Azhar. All rights reserved.
//

import UIKit

class WASearchViewController: UITableViewController, UISearchControllerDelegate, UISearchBarDelegate {

    private var artists: WAArtistsList?
    
    private var artistsInfo: [WAArtistInfo] = []
    
    private var paintings: WAPaintingsList?
    
    private var selectedScope = 0
    
    private var searchController = UISearchController(searchResultsController: nil)
    
    private var selection = UISegmentedControl(items: ["Artists", "Paintings"])

    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        super.loadView()
        tableView = UITableView(frame: .zero, style: .grouped)
        tableView.delegate = self
        tableView.dataSource = self
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // It is usually good to set the presentation context.

        definesPresentationContext = true
        searchController.dimsBackgroundDuringPresentation = false
        searchController.delegate = self
        searchController.searchBar.delegate = self
        navigationItem.hidesSearchBarWhenScrolling = false
        navigationItem.searchController = searchController
        navigationItem.title = "Search"
        
        searchController.searchBar.tintColor = UIColor.black
        updateErrorMessage(term: "")
        
        tableView.backgroundColor = .white
        tableView.register( WAPaintingSearchResultTableViewCell.self, forCellReuseIdentifier: WAPaintingSearchResultTableViewCell.self.description() )
        tableView.register( WAPaintingTitleTableViewSectionFooterView.self, forHeaderFooterViewReuseIdentifier: WAPaintingTitleTableViewSectionFooterView.self.description() )

        tableView.register( UINib.artistTableViewCell, forCellReuseIdentifier: UINib.NibName.ArtistTableViewCell  )
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        searchController.searchBar.scopeButtonTitles = ["Paintings", "Artists"]
    }
    
    func updateErrorMessage(term: String? = nil) {
        if term == "" {
            self.messageLabel.text = "Enter Search Term To Begin"
        }
        else if selectedScope == 1 && artistsInfo.count == 0 {
            self.messageLabel.text = "No Artist Found"
        }
        else if selectedScope == 0 && ( paintings?.Paintings.count == 0  || paintings == nil ){
            self.messageLabel.text = "No Artworks Found"
        }
        else {
            self.messageLabel.text = nil
        }
    
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        tableView.tableHeaderView = nil
        if let term = searchBar.text {
            searchFor(string: term)
            
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.count == 0 {
            searchBarCancelButtonClicked(searchBar)
        }
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.paintings = nil
        self.artists = nil
        self.artistsInfo = []
        updateErrorMessage(term: "")
        self.tableView.reloadData()
    }
    
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        self.selectedScope = selectedScope
        reloadData(scope: selectedScope)
        updateErrorMessage(term: searchBar.text)
    }
    
    func searchFor(string:String) {
        
        self.messageLabel.text = nil
        self.paintings = nil
        self.artists = nil
        self.artistsInfo = []
        self.tableView.reloadData()
        
        let searchString = string.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""

        activityIndicator.startAnimating()
        
        Current.service.getArtworksSearchResults(for: searchString) { [weak self] (list) in
                self?.paintings = list
                self?.reloadData(scope:0)
                self?.activityIndicator.stopAnimating()
                self?.updateErrorMessage()
        }
        
        Current.service.getArtistSearchResults(for: searchString) { [weak self] (list) in
            self?.artists = list
            self?.artistsInfo = list?.Artists ?? []
            self?.reloadData(scope:1)
            self?.updateErrorMessage()
            self?.activityIndicator.stopAnimating()
        }
        
    }
    
    func reloadData(scope:Int){
        if self.selectedScope == scope {
            self.tableView.reloadData()
        }

    }
}

extension WASearchViewController {
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if selectedScope == 1 {
            
            let artist = artistsInfo[indexPath.row]
            
            let bioUrl: WAUrl = .parts(Current.language, WAUrlLocation.artist(artist.artistEndPoint), WAUrlParameters.singleValue(.jsonV2))
            
            let resource = Resource<WAArtistPage>.init(url: bioUrl)
            
            let loadingVC = WALodingResourceViewController(resource: resource) { artistPage in
                let vc = UIStoryboard.init(name: "WAArtistDetails", bundle: nil).instantiateInitialViewController() as! WAArtistDetailsTableViewController
                vc.artistPage = artistPage
                return vc

            }
            
            show(loadingVC, sender: self)
            
        }

        else if selectedScope == 0 {
            guard let painting = paintings?.Paintings[indexPath.section] else { return }
            let vc = WAArtworkDetailsViewController(artwork: painting)
            show(vc, sender: self)
     }

    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        var sections = 0
        switch selectedScope {
        case 0:
            sections = paintings?.Paintings.count ?? 0
        case 1:
            sections = 1
        default:
            fatalError()
        }

        return sections
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch selectedScope {
        case 0:
            return 1
        case 1:
            return artistsInfo.count
        default:
            fatalError()
        }
        
        
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        switch selectedScope {
        case 0:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: WAPaintingSearchResultTableViewCell.self.description() ) as! WAPaintingSearchResultTableViewCell
            if let p = paintings?.Paintings[indexPath.section] {
                cell.configure(forPainting: p)
            }
            
            return cell
        
        case 1:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: UINib.NibName.ArtistTableViewCell ) as! WAArtistTableViewCell
                cell.configure(forArtist: artistsInfo[indexPath.row])
                cell.accessoryType = .disclosureIndicator
            
            return cell
        
        default:
            fatalError()
        }
        
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if selectedScope != 0 { return 54 }
        
        if let p = paintings?.Paintings[indexPath.section] {
            let aspect = p.height / p.width
            return (tableView.frame.width - 8) * aspect
        }
        
        return 1
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 30.0
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if selectedScope != 0 { return nil}
        let footer = tableView.dequeueReusableHeaderFooterView(withIdentifier: WAPaintingTitleTableViewSectionFooterView.self.description() ) as! WAPaintingTitleTableViewSectionFooterView
        if let p = paintings?.Paintings[section] { footer.configureFor(painting: p) }
        return footer
    }
    
}
