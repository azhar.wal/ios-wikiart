//
//  WaArtistModels.swift
//  WikiArt2.0
//
//  Created by Waleed Azhar on 2017-09-24.
//  Copyright © 2017 Waleed Azhar. All rights reserved.
//

import Foundation
import UIKit

struct WAArtistInfo: Codable {
    
    var id: String?
    var title: String?
    var year: String?
    var nation: String?
    var image: String?
    var artistUrl: String?
    var totalWorksTitle: String?
    var url: String?
    
    var artistEndPoint: String {
        return String(artistUrl?.split(separator: Character("/")).last ?? "")
    }
}

class WAArtistsList: Codable {
    
    var ArtistsHtml: String?
    var canLoadMoreArtists: Bool {
        return AllArtistsCount > Artists.count
    }
    var Paintings: [WAPainting]?
    var PaintingsHtml: String?
    var PaintingsHtmlBeta: String?
    var AllPaintingsCount: Int?
    var AllArtistsCount: Int
    var PageSize: Int
    var timeLog: String?
    var listURL:String?
    var Artists: [WAArtistInfo]
    var isLoading: Bool?
    
}

class WAArtistPage: Codable {
    
    var activeYearsCompletion:Float?
    var activeYearsStart:Float?
    private var artistName: String? = ""
    var artistNamed: String? {
        get {
            return artistName?.convertFromHTMLUnicode
        }
    }
    
    var biography: String? = ""
    var birthDay:String?
    var birthDayAsString: String?
    var contentId: Int?
    var deathDay: String?
    var deathDayAsString: String?
    var dictonaries:[Int]?
    var gender: String?
    var image: String?
    var lastNameFirst: String?
    var OriginalArtistName: String?
    var periodsOfWork: String?
    var relatedArtistsIds:[Int]?
    var series: String?
    var story: String?
    var themes: String?
    var url: String? = ""
    var year: String?
    var nation: String?
    var totalWorksTitle: String?
    
    var artistEndPoint: String {
        return String(url?.split(separator: Character("/")).last ?? "")
    }
    
    var details: String {
        return (birthDayAsString ?? "") + " to " + (deathDayAsString ?? "")
    }
    
    
}


extension WAArtistPage: Hashable {
    
    static func == (lhs: WAArtistPage, rhs: WAArtistPage) -> Bool {
        return lhs.url == rhs.url
    }
    
    public var hashValue: Int {
        return artistName!.hashValue ^ biography!.hashValue ^ url!.hashValue
    }
    
}
