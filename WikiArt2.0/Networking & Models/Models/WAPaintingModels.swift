//
//  Models.swift
//  WikiArt2.0
//
//  Created by Waleed Azhar on 2017-09-24.
//  Copyright © 2017 Waleed Azhar. All rights reserved.
//

import Foundation
import UIKit

class WAPaintingsList: Codable {
    
    var ArtistsHtml: String?
    var CanLoadMoreArtists: Bool = false
    var Paintings: [WAPainting] = []
    var paintingsHTML: String?
    var AllPaintingsCount: Int = 0
    var PageSize: Int = 0
    var timeLog: String?
    
    var canLoadMorePaintings:Bool {
        return AllPaintingsCount > Paintings.count
    }
    
    var remainingPages: Int {
        return Int( AllPaintingsCount.double / Paintings.count.double )
    }
    
    enum CodingKeys: CodingKey {
        case ArtistsHtml
        case CanLoadMoreArtists
        case paintingsHTML
        case AllPaintingsCount
        case PageSize
        case timeLog
        case Paintings
    }
    
}

class WAPainting: Codable {
  
    private var title: String = ""
    var titled: String {
        get {
            return title.convertFromHTMLUnicode
        }
    }
    
    private var artistName: String = ""
    var artistNamed: String {
        get {
            return artistName.convertFromHTMLUnicode
        }
    }
    
    var id: String = ""
    var year: String = ""
    var width: CGFloat = 0.0
    var height: CGFloat = 0.0
    var image: String = ""
    var map: String = ""
    var paintingUrl: String = ""
    var flags: Int = 0
    var imageUI:UIImage? = nil
    
    var artistInfo: String {
        return "\(artistName) ∙ \(year)"
    }
    
    var artistEndPoint: String {
        return String( paintingUrl.split (separator: Character("/") ).dropLast().last ?? "" )
    }
    
    enum CodingKeys:String, CodingKey {
        case id
        case title
        case year
        case width
        case height
        case artistName
        case image
        case map
        case paintingUrl
        case flags
    }
    
}

extension WAPainting {
    var aspectRatio: CGFloat {
        return height / width
    }
}

extension WAPainting: Hashable {
   
    static func == (lhs: WAPainting, rhs: WAPainting) -> Bool {
        return lhs.id == rhs.id
    }
    
    public var hashValue: Int {
        return id.hashValue ^ title.hashValue ^ year.hashValue
    }
    
}

enum WAPaintingSize:String {
    case blog = "!Blog.jpg"
    case large = "!Large.jpg"
    case smallPortrait = "!PortraitSmall.jpg"
    case portrait = "!Portrait.jpg"
    case halfHD = "!HalfHD.jpg"
    case HD = "!HD.jpg"
    case small = "!PinterestSmall.jpg"
    case medium = "!PinterestLarge.jpg"
    case original = ""
    
    static func change(artwork:String, to size:WAPaintingSize) -> String{
        var s = artwork
        let ss = ["!Blog.jpg", "!Large.jpg", "!PortraitSmall.jpg", "!Portrait.jpg", "!HalfHD.jpg", "!HD.jpg", "!PinterestSmall.jpg","!PinterestLarge.jpg"]
        
        for i in ss{
            if s.hasSuffix(i) {
                s = s.replacingOccurrences(of: i, with: "")
            }
        }
        return s + size.rawValue
    }
}

