//
//  ArtWorksCategoryTypes.swift
//  WikiArt2.0
//
//  Created by Waleed Azhar on 2018-04-15.
//  Copyright © 2018 Waleed Azhar. All rights reserved.
//

import Foundation

struct ArtworksTypeServer: Decodable {
    
    struct ContentType: Decodable {
      
        struct TitleContent: Decodable {
            let Title: [String:String]
        }
        
        let Title: TitleContent
        let Status: Int
        let Group: Int
    }
    
    var title: String? {
        get {
            return self.Content.Title.Title[Current.language.rawValue]?.capitalized ?? self.Content.Title.Title["en"]?.capitalized
        }
    }
    
    let _id: ID
    let Content: ContentType
}
