
//
//  ArtistTypes.swift
//  WikiArt2.0
//
//  Created by Waleed Azhar on 2018-04-15.
//  Copyright © 2018 Waleed Azhar. All rights reserved.
//

import Foundation

struct ID: Decodable {
    let _oid: String
}

struct ArtistsDictionariesWithCategories: Decodable {
    let CategoryId: ID
    let Url: String 
    let Title: String
    let Count: Int
    
    var url: String {
        return String(Url.split(separator: Character(unicodeScalarLiteral: "/")).last ?? "")
    }
    
    
}

struct WAArtistsTypes: Decodable {

    struct ArtistTypesKey: Decodable {

        struct ContentType: Decodable {
            struct TitleContent: Decodable {
                let Title: [String:String]
            }
            
            let Title: TitleContent
        }
        
        let _id: ID
        let Content: ContentType
    }
    
    let Categories:[ArtistTypesKey]
    let DictionariesWithCategories: [String: [ArtistsDictionariesWithCategories]]
    let Group: Int
    let Menu: String?
    let SortBy: Int
}

extension WAArtistsTypes {
    
    var items: [ArtistsDictionariesWithCategories] {
        get { return DictionariesWithCategories.values.flatMap { return $0 } }
    }
    
}

