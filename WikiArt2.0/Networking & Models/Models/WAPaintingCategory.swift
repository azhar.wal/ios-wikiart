//
//  WAPaintingCategory.swift
//  Cells
//
//  Created by Waleed Azhar on 9/28/17.
//  Copyright © 2017 Waleed Azhar. All rights reserved.
//

import Foundation

enum WAArtworksCategory: String {
    
    static let parameterForCategory: [ WAArtworksCategory : WAUrlParamerterValue ] = [ .style : .artworkStyles, .genre: .artworkGenres, .media : .artworkMedia ]
    
    case media = "paintings-by-media"
    case style = "paintings-by-style"
    case genre = "paintings-by-genre"
    case recent = "recently-added-artworks"
    case random =  "App/Painting/random"
    case popular = "popular-paintings/alltime"
    case featured = ""
    case favorites = "favs"
}


let titles: [ WALanguage : [ WAArtworksCategory : String ] ] = [
    .en : [ .featured:"Featured", .style: "Style", .genre:"Genres", .recent:"Recently added", .popular:"Popular artworks", .media:"Media"],
    .de : [ .featured:"Angesagt", .style: "Stile", .genre:"Genres", .recent:"Vor kurzem hinzugefügt", .popular:"Beliebt", .media:"Medien"],
    .es : [ .featured:"Destacado", .style: "Estilos", .genre:"Géneros", .recent:"Últimos аñadidos", .popular:"Popular artworks", .media:"Media"],
    .uk : [ .featured:"Вибране", .style: "Стилі", .genre:"Жанри", .recent:"Нове", .popular:"Популярні твори", .media:"Медіа"],
    .fr : [ .featured:"En Vedette", .style: "Styles", .genre:"Genres", .recent:"Ajouté récemment", .popular:"Œuvres populaires", .media:"Médias"],
    .pt : [ .featured:"Destaque", .style: "Estilos", .genre:"Gêneros", .recent:"Recentemente adicionado", .popular:"Popular artworks", .media:"Mídia"],
    .ru : [ .featured:"Избранное", .style: "Стили", .genre:"Жанры", .recent:"Новые", .popular:"Популярные", .media:"Медиа"]
]

extension WAArtworksCategory {

    func title() -> String {
        switch self {
        case .favorites : return "Favorites"
        case .random : return "Random"
        default : return titles[Current.language]![self]!
        }
    }
    
}
