//
//  WikiArtAService.swift
//  WikiArt2.0
//
//  Created by Waleed Azhar on 2018-10-16.
//  Copyright © 2018 Waleed Azhar. All rights reserved.
//

import Foundation

class WikiArtService: ServiceType {

    var language: WALanguage
    
    var resourceFactory: ResourceFactory
    
    var webService: WebService
    
    required init(language: WALanguage, resourceFactory: ResourceFactory, webService: WebService ) {
        self.language = language
        self.resourceFactory = resourceFactory
        self.webService = webService
    }
    
}

class WikiArtResources: ResourceFactory {
    var urlFactory: WAUrlFactory = WikiArtUrlFactory()
}

class WikiArtUrlFactory: WAUrlFactory { }
