//
//  WAArtworkListManager.swift
//  WikiArt2.0
//
//  Created by Waleed Azhar on 2018-04-16.
//  Copyright © 2018 Waleed Azhar. All rights reserved.
//

import Foundation

enum WAPageState {
    case blank, first, intermediate, final
}

enum WAArtworksDataSourceType {
    case allPaintings, category, favorites
}


final class WAArtworksDataSource: NSObject {
    
    public private(set) var paintingsList: WAPaintingsList?
    
    public private(set) var type: WAArtworksDataSourceType
    
    public private(set) var category: WAArtworksCategory
    
    public private(set) var categoryItem: ArtworksTypeServer?
    
    public private(set) var artistPage: WAArtistPage?
    
    public private(set) var currentPage = 1
    
    public var count: Int {
        return paintingsList?.Paintings.count ?? 0
    }
    
    public var canLoadMorePaintings: Bool {
        return paintingsList?.canLoadMorePaintings ?? false
    }
    
    public var completion: (Int) -> () = { (page: Int) in }
    
    public var error: () -> () = { }
    
    init(favorites favs:WAArtworksDataSourceType = .favorites, completion: @escaping (Int) -> () ) {
        self.type = favs
        self.artistPage = nil
        self.category = .media
        self.categoryItem = nil
        self.paintingsList = WAPaintingsList()
        self.completion = completion
        
        self.paintingsList!.Paintings = Current.favorites.paintingsSorted
        self.paintingsList!.AllPaintingsCount = 0
    }
    
    init(allPaintings artistPage: WAArtistPage ) {
        self.type = .allPaintings
        self.artistPage = artistPage
        self.category = .media
        self.categoryItem = nil
        super.init()
        
        Current.service.getAllPaintingsList(for: artistPage, page: currentPage ) { [weak self] list in
            if let list = list {
                self?.paintingsList = list
                self?.completion(1)
            }
                
            else { self?.error() }
        }

    }

    init( category: WAArtworksCategory, categoryItem: ArtworksTypeServer? ) {
        self.type = .category
        self.category = category
        self.categoryItem = categoryItem
        super.init()
        
        Current.service.getPaintingsList(for: category, item: categoryItem, page: 1) { [weak self] list in
            if let list = list {
                self?.paintingsList = list
                self?.completion(1)
            }
            else { self?.error() }
        }

    }
    
    public func loadNextPage() {
        
        guard let list = paintingsList, list.canLoadMorePaintings else { return }
        
        if  type == .allPaintings, let artist = artistPage {
            //TODO: refactor
            Current.service.getAllPaintingsList(for: artist, page: currentPage + 1) { [weak self] list in
                if let l = list, let this = self {
                    this.paintingsList!.Paintings.append(contentsOf: l.Paintings)
                    this.currentPage = this.currentPage + 1
                    this.completion(this.currentPage)
                }
            }
        }
            
        else{
            Current.service.getPaintingsList(for: category, item: categoryItem, page: currentPage + 1) { [weak self] list in
                if let l = list, let this = self {
                    this.paintingsList!.Paintings.append(contentsOf: l.Paintings)
                    this.currentPage = this.currentPage + 1
                    this.completion(this.currentPage)
                }
            }
        }
    }
    
}
