//
//  WAArtistList.swift
//  WikiArt2.0
//
//  Created by Waleed Azhar on 2018-10-09.
//  Copyright © 2018 Waleed Azhar. All rights reserved.
//

import Foundation

class WAArtistsDataSource: NSObject {

    public private(set) var artists: WAArtistsList? = nil
    
    public private(set) var category: WAArtistsCategories
    
    public private(set) var item: ArtistsDictionariesWithCategories?
    
    public private(set) var currentPage = 1
    
    public var completion: (Int) -> () = { (page: Int) in }
    
    public var error: () -> () = { }
    
    var count: Int {
        return artists?.Artists.count ?? 0
    }
    
    init(category: WAArtistsCategories, item: ArtistsDictionariesWithCategories?) {
        self.category = category
        self.item = item
        super.init()
        Current.service.getArtistsList(for: category, item: item, page: currentPage) { [weak self] list in
            if let list = list {
                self?.artists = list
                self?.loadNextPage()
            }
            else { self?.error() }
        }
        
    }
    
    public func loadNextPage() {
        
        guard let list = artists, list.canLoadMoreArtists else { completion(currentPage); return }
        
        Current.service.getArtistsList(for: category, item: item, page: currentPage + 1)  { [weak self] list in
                if let list = list, let this = self {
                    this.artists!.Artists.append(contentsOf: list.Artists)
                    this.currentPage = this.currentPage + 1
                    this.loadNextPage()
                }
        }
        
    }
}

