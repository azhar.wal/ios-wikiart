 //
//  CategoryManagers.swift
//  WikiArtTV
//
//  Created by Waleed Azhar on 2018-10-04.
//  Copyright © 2018 Waleed Azhar. All rights reserved.
//

import Foundation
 //TODO: Bad Design
 
struct ArtistsCategoriesDataSource {
    
    static let artistsCategories: [WAArtistsCategories] =  [ .popular, .female, .recent, .artMovement, .school, .genre, .field, .nation, .centuries, .chronology, .institutions]
    
    static let categoriesWithTypes: [WAArtistsCategories] = [.artMovement, .school, .genre, .field, .nation, .centuries, .institutions]

}


struct ArtworksCategoriesDataSource {
    
    static let artworksCategories: [WAArtworksCategory] = [ .favorites, .featured, .popular, .style, .genre, .recent, .media ]
    
    static let categoriesWithTypes: [ WAArtworksCategory] = [ .style, .genre, .media ]
    
}



