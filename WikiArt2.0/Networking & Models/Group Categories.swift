//
//  Group Categories.swift
//  WikiArt2.0
//
//  Created by Waleed Azhar on 2018-10-15.
//  Copyright © 2018 Waleed Azhar. All rights reserved.
//

import Foundation

enum WAArtistsCategories: String {
    case alphabetical = "Alphabet"
    case artMovement = "artists-by-art-Movement"
    case school = "artists-by-painting-school"
    case genre = "artists-by-genre"
    case field = "artists-by-field"
    case nation = "artists-by-nation"
    case centuries = "artists-by-century"
    case chronology = "chronological-artists"
    case popular = "popular-artists"
    case female = "female-artists"
    case recent = "recently-added-artists"
    case institutions = "artists-by-art-institution"
}

extension WAArtistsCategories {
    // TODO: clean up
    func title() -> String {
        return self.rawValue.replacingOccurrences(of: "artists-by-", with: "").replacingOccurrences(of: "-artists", with: "").replacingOccurrences(of: "-", with: " ").capitalized
    }
    
}
