//
//  WebService.swift
//  WikiArt2.0
//
//  Created by Waleed Azhar on 2017-09-24.
//  Copyright © 2017 Waleed Azhar. All rights reserved.
//

import Foundation
import UIKit

extension NSMutableURLRequest {
    
    convenience init<A>(resource: Resource<A>) {
        self.init(url: resource.url as URL)
        self.httpMethod = resource.method.method
        if case let .post(data) = resource.method {
            setValue("application/json", forHTTPHeaderField: "Content-Type")
            httpBody = data as Data
        }
    }
    
}

extension NSURL {
    
    convenience init?(url: WAUrl) {
        self.init(string: url.reduce())
    }
    
}

struct Resource<A> {
    
    var id: Int? = nil
    let url: NSURL
    let method: HttpMethod<Data> = .get
    
}

extension Resource {
    
    init(url: WAUrl) {
        self.url = NSURL(url: url)!
    }
    
    init(url: NSURL) {
        self.url = url
    }
    
}

extension Resource: Hashable {
    
    static func == (lhs: Resource<A>, rhs: Resource<A>) -> Bool {
        return lhs.url == rhs.url && lhs.method.method == rhs.method.method
    }
    
    public var hashValue: Int {
        return url.hash & method.method.hash
    }
    
    
}

struct ComplexResource<A: Decodable> {
    
    typealias Element = A
    
    var resources: [Resource<Element>]
    
    var group = DispatchGroup()
    
    var queue = DispatchQueue.global()
    
}

extension ComplexResource {
    init(resources: [Resource<Element>]) {
        self.resources = resources
    }
}

final class WebService {
    
    func load<A: Decodable>(resource: Resource<A>, dispatchQueue:DispatchQueue = DispatchQueue.main, completion: @escaping (A?) -> () ) {
        
        let request = NSMutableURLRequest(resource: resource)
        
        URLSession.shared.dataTask(with: request as URLRequest) { (data, _, _) in
            guard let data = data else {
                dispatchQueue.sync { completion(nil) }
                return
            }
            
            let decoder = JSONDecoder()
            do {
                let page = try decoder.decode(A.self, from: data)
                dispatchQueue.sync { completion(page) }
            } catch {
                dispatchQueue.sync { completion(nil) }
            }
        }.resume()
    }
}

enum HttpMethod<JSON> {
    case get
    case post(JSON)
}

extension HttpMethod {
    
    var method: String {
        switch self {
        case .get: return "GET"
        case .post: return "POST"
        }
    }
    
    func map<DATA>(f: (JSON) -> DATA) -> HttpMethod<DATA> {
        switch self {
        case .get: return .get
        case .post(let body):
            return .post(f(body))
        }
    }
}

