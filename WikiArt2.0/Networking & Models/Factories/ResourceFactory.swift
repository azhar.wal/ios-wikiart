//
//  ResourceFactory.swift
//  WikiArt2.0
//
//  Created by Waleed Azhar on 2018-10-16.
//  Copyright © 2018 Waleed Azhar. All rights reserved.
//

import Foundation

protocol ResourceFactory {
    
    var urlFactory: WAUrlFactory { get }
    
    func relatedPaintings(for painting: WAPainting) -> Resource<WAPaintingsList>
    
    func featuredPaintings(for artist: WAArtistPage) -> Resource<WAPaintingsList>
    
    func allPaintingsList(for artist: WAArtistPage, page: Int) -> Resource<WAPaintingsList>
    
    func paintingsList(for category:WAArtworksCategory, item: ArtworksTypeServer?, page: Int) -> Resource<WAPaintingsList>
    
    func artistsList(for category: WAArtistsCategories, item: ArtistsDictionariesWithCategories?, page: Int) -> Resource<WAArtistsList>
    
    func artistPage(for artist: WAArtistInfo) -> Resource<WAArtistPage>

    func artistSearchResults(for term: String ) -> Resource<WAArtistsList>
    
    func artworksSearchResults(for term: String ) -> Resource<WAPaintingsList>
    
    func categories(for artworksCategory: WAArtworksCategory)  -> Resource< [ArtworksTypeServer] >
    
    func categories(for artistCategory: WAArtistsCategories)  -> Resource<WAArtistsTypes>

}

extension ResourceFactory {
    
    func relatedPaintings(for painting: WAPainting) -> Resource<WAPaintingsList> {
        return Resource<WAPaintingsList>(url: urlFactory.relatedPaintingsUrl(for: painting))
    }
    
    func featuredPaintings(for artist: WAArtistPage) -> Resource<WAPaintingsList> {
        return Resource<WAPaintingsList>(url: urlFactory.featuredPaintingsUrl(for: artist))
    }
   
    func allPaintingsList(for artist: WAArtistPage, page: Int) -> Resource<WAPaintingsList> {
        return Resource<WAPaintingsList>(url: urlFactory.allPaintingsUrl(for: artist, page: page))
    }
    
    func paintingsList(for category:WAArtworksCategory, item: ArtworksTypeServer?, page: Int) -> Resource<WAPaintingsList> {
        return Resource<WAPaintingsList>(url: urlFactory.paintings(for: category, item: item, page: page))
    }
    
    func artistsList(for category: WAArtistsCategories, item: ArtistsDictionariesWithCategories?, page: Int) -> Resource<WAArtistsList> {
        return Resource<WAArtistsList>(url: urlFactory.artistsList(for: category, item: item, page: page))
    }
    
    func artistPage(for artist: WAArtistInfo) -> Resource<WAArtistPage> {
        return Resource<WAArtistPage>( url: urlFactory.artistPageUrl(for: artist))
    }
    
    func artistSearchResults(for term: String ) -> Resource<WAArtistsList> {
        return Resource<WAArtistsList>(url: urlFactory.urlSearchForArtist(with: term))
    }
    
    func artworksSearchResults(for term: String ) -> Resource<WAPaintingsList> {
        return Resource<WAPaintingsList>(url: urlFactory.urlSearchForArt(with: term))
    }
    
    func categories(for artworksCategory: WAArtworksCategory)  -> Resource<[ArtworksTypeServer]> {
        return Resource<[ArtworksTypeServer]>(url: urlFactory.url(for: artworksCategory))
    }
    
    func categories(for artistCategory: WAArtistsCategories)  -> Resource<WAArtistsTypes> {
        return Resource<WAArtistsTypes>(url: urlFactory.url(for: artistCategory))
    }
    
}
