//
//  WAUrl.swift
//  WikiArt2.0
//
//  Created by Waleed Azhar on 2018-10-10.
//  Copyright © 2018 Waleed Azhar. All rights reserved.
//

import Foundation

enum WAArtistsCategories: String {
    case alphabetical = "Alphabet"
    case artMovement = "artists-by-Art-Movement"
    case school = "artists-by-painting-school"
    case genre = "artists-by-genre"
    case field = "artists-by-field"
    case nation = "artists-by-nation"
    case centuries = "artists-by-century"
    case chronology = "chronological-artists"
    case popular = "popular-artists"
    case female = "female-artists"
    case recent = "recently-added-artists"
    case institutions = "artists-by-art-institution"
}

extension WAArtistsCategories {
    // TODO: clean up
    func title() -> String {
        return self.rawValue.replacingOccurrences(of: "artists-by-", with: "").replacingOccurrences(of: "-artists", with: "").replacingOccurrences(of: "-", with: " ").capitalized
    }
    
}


enum WAGroupCategories: String {
    case dictionary = "app/dictionary/GetAllGroup"
    case advancedSearch =  "app/Search/PaintingAdvancedSearch"
}

// TODO: refactor
enum WAUrlParamerterValue: String {
    case jsonV2 = "json=2"
    case layout = "layout=new"
    case jsonV3 = "json=3"
    case featuredArtworks = "param=featured"
    case sortByName = "sort=2"
    case sortByCount = "sort=3"
    case sortByFeatured = "select=featured"
    case sortByAllWorks = "select=all-works"
    case artworkStyles = "group=2"
    case artworkGenres = "group=3"
    case artworkMedia = "group=12"
}

enum WAUrlParamerterVariable {
    
    case page(Int)
    case searchID(String)
    case pageSize(Int)
    case searchTerm(String)
    
    func reduce() -> String {
        switch self {
        case .pageSize(let pageSize):
            return "pageSize=\(pageSize)"
        case .page(let pageInt):
            return "page=\(pageInt)"
        case .searchID(let id):
            return "dictIdsJson=%5B%22\(id)%22%5D"
        case .searchTerm(let term):
            return "searchterm=\(term)"
        }
        
    }
}

enum WALanguage: String {
    case en, de, es, uk, fr, pt, ru
}

indirect enum WAUrlParameters {
    case singleValue(WAUrlParamerterValue)
    case singleVariable(WAUrlParamerterVariable)
    case multipleValue(WAUrlParamerterValue, WAUrlParameters)
    case multipleVariable(WAUrlParamerterVariable, WAUrlParameters)
}

extension WAUrlParameters {
    func reduce() -> String {
        switch self {
        case .singleValue(let param):
            return "\(param.rawValue)"
        case .singleVariable(let param):
            return param.reduce()
        case .multipleValue(let singleParam, let multiParam):
            return "\(singleParam.rawValue)&\(multiParam.reduce())"
        case .multipleVariable(let var1, let multiParam):
            return "\(var1.reduce())&\(multiParam.reduce())"
        }
    }
}

struct WAArtworksType {
    let url = "artwork"
}

struct WAArtistsType {
    let url = "artist"
}

enum WAUrlLocation {
    case artists(WAArtistsCategories, ArtistsDictionariesWithCategories? )
    case artworks(WAArtworksCategory, WAArtworksType?)
    case app(WAGroupCategories)
    case featuredPaintingForArtist(String)
    case allPainting(String)
    case relatedArtworks(String)
    case artist(String)
    case search(String)
}

extension WAUrlLocation {
    func reduce() -> String {
        switch self {
        case .artist(let artist):
            return artist
        case .artists(let cat, _):
            return "App/Search/\(cat.rawValue)"
        case .artworks(let cat, let type):
            return "\(cat.rawValue)/\(type?.url ?? "")"
        case .app(let cat):
            return "\(cat.rawValue)"
        case .allPainting(let artist):
            return "\(artist)/mode/all-paintings"
        case .relatedArtworks(let artworkUrl):
            return artworkUrl.replacingOccurrences(of: "/\(Current.language.rawValue)/", with: "")
        case .search(let term):
            return "/Search/\(term)"
        case .featuredPaintingForArtist(let artist):
            return "\(artist)/mode/featured"
        }
    }
}

enum WAUrl {
    case parts(WALanguage, WAUrlLocation, WAUrlParameters?)
    case whole(String, WAUrlParameters?)
}

extension WAUrl {
    
    func reduce() -> String {
        let domain = "https://www.wikiart.org"
        switch self {
        case .parts(let lang, let loc, let param):
            return "\(domain)/\(lang)/\(loc.reduce())?\(param?.reduce() ?? "")"
        case .whole(let url, let param):
            return "\(domain)\(url)?\(param?.reduce() ?? "")"
        }
    }
    
}

func makeUrl(forCategory category:WAArtworksCategory, item: ArtworksTypeServer?, page: Int = 1) -> WAUrl {
    
    switch category {
    case .favorites: return .parts(Current.language, .artworks(category,nil), nil)
    case .featured, .random:
        return .parts(Current.language, .artworks(category, nil), .multipleValue(.jsonV2, .multipleValue(.featuredArtworks, .singleVariable(.page(page)))))
    case .recent, .popular:
        return .parts(Current.language, .artworks(category, nil), .multipleValue(.jsonV2, .singleVariable(.page(page))))
    case .genre, .media, .style:
        return .parts(Current.language, .app(.advancedSearch), .multipleValue(.jsonV2, .multipleVariable(.searchID(item!._id._oid), .singleVariable(.page(page)))))
    }
    
}

func artistsListUrl(category: WAArtistsCategories, item: ArtistsDictionariesWithCategories?, page: Int) -> WAUrl {
    
    return .parts(Current.language, .artists(category, item), .multipleValue(.jsonV3, .multipleValue(.layout, .multipleVariable(.page(page), .singleVariable( .searchTerm(item?.url ?? "" ))))))
    
}
