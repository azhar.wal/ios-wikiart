//
//  ServiceType.swift
//  WikiArt2.0
//
//  Created by Waleed Azhar on 2018-10-16.
//  Copyright © 2018 Waleed Azhar. All rights reserved.
//

import Foundation

protocol ServiceType {
    
//    var appId: String { get }
//
//    var serverConfig: ServerConfigType { get }
//
//    var oauthToken: OauthTokenAuthType? { get }
    
    var language: WALanguage { get }
    
//    var buildVersion: String { get }
    
    var resourceFactory: ResourceFactory { get }
    
    var webService: WebService { get }
    
    init(//appId: String,
        // serverConfig: ServerConfigType,
      //   oauthToken: OauthTokenAuthType?,
         language: WALanguage,
//         buildVersion: String,
         resourceFactory: ResourceFactory,
         webService: WebService)
    
    func getAllPaintingsList(for artist: WAArtistPage, page: Int, completion: @escaping ( WAPaintingsList? ) -> () )
    
    func getPaintingsList(for category:WAArtworksCategory, item: ArtworksTypeServer?, page: Int, completion: @escaping ( WAPaintingsList? ) -> () )
   
    func getArtistsList(for category: WAArtistsCategories, item: ArtistsDictionariesWithCategories?, page: Int, completion: @escaping ( WAArtistsList? ) -> () )
    
    func getArtistPage(for artist: WAArtistInfo, completion: @escaping ( WAArtistPage? ) -> () )
    
    func getArtistSearchResults(for term: String, completion: @escaping ( WAArtistsList? ) -> () )
    
    func getArtworksSearchResults(for term: String, completion: @escaping ( WAPaintingsList? ) -> () )
    
    func getCategories(for artworksCategory: WAArtworksCategory, completion: @escaping ( [ArtworksTypeServer]? ) -> () )
    
    func getCategories(for artistCategory: WAArtistsCategories, completion: @escaping ( WAArtistsTypes? ) -> () )
    
    func getRelatedPaintings(for painting: WAPainting, completion: @escaping ( WAPaintingsList? ) -> () )
    
    func getFeaturedPaintings(for artist: WAArtistPage, completion: @escaping ( WAPaintingsList? ) -> () )
}

extension ServiceType {
    
    
    func getAllPaintingsList(for artist: WAArtistPage, page: Int, completion: @escaping ( WAPaintingsList? ) -> ()) {
        webService.load(resource: resourceFactory.allPaintingsList(for: artist, page: page), completion: completion)
    }
    
    func getPaintingsList(for category:WAArtworksCategory, item: ArtworksTypeServer?, page: Int, completion: @escaping ( WAPaintingsList? ) -> ()) {
        webService.load(resource:resourceFactory.paintingsList(for: category, item: item, page: page), completion: completion)
    }
    
    func getArtistsList(for category: WAArtistsCategories, item: ArtistsDictionariesWithCategories?, page: Int, completion: @escaping ( WAArtistsList? ) -> () ) {
        webService.load(resource: resourceFactory.artistsList(for: category, item: item, page: page), completion: completion)
    }
    
    func getArtistPage(for artist: WAArtistInfo, completion: @escaping ( WAArtistPage? ) -> () ) {
        webService.load(resource: resourceFactory.artistPage(for: artist), completion: completion)
    }
    
    func getArtistSearchResults(for term: String, completion: @escaping ( WAArtistsList? ) -> () ) {
        webService.load(resource: resourceFactory.artistSearchResults(for: term), completion: completion)
    }
    
    func getArtworksSearchResults(for term: String, completion: @escaping ( WAPaintingsList? ) -> () ) {
        webService.load(resource: resourceFactory.artworksSearchResults(for: term), completion: completion)
    }
    
    func getCategories(for artworksCategory: WAArtworksCategory, completion: @escaping ( [ArtworksTypeServer]? ) -> () ) {
        webService.load(resource: resourceFactory.categories(for: artworksCategory), completion: completion)
    }
    
    func getCategories(for artistCategory: WAArtistsCategories, completion: @escaping ( WAArtistsTypes? ) -> () ) {
        webService.load(resource: resourceFactory.categories(for: artistCategory), completion: completion)
    }
    
    func getRelatedPaintings(for painting: WAPainting, completion: @escaping ( WAPaintingsList? ) -> () ) {
        webService.load(resource: resourceFactory.relatedPaintings(for: painting), completion: completion)
    }
    
    func getFeaturedPaintings(for artist: WAArtistPage, completion: @escaping ( WAPaintingsList? ) -> () ) {
        webService.load(resource: resourceFactory.featuredPaintings(for: artist), completion: completion)
    }
}

