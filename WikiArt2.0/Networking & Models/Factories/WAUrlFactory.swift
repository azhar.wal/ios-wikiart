//
//  WAUrlGenerator.swift
//  WikiArt2.0
//
//  Created by Waleed Azhar on 2018-10-16.
//  Copyright © 2018 Waleed Azhar. All rights reserved.
//

import Foundation

protocol WAUrlFactory {
    
    func relatedPaintingsUrl(for painting: WAPainting) -> WAUrl
    
    func featuredPaintingsUrl(for artist: WAArtistPage) -> WAUrl
    
    func paintings(for category:WAArtworksCategory, item: ArtworksTypeServer?, page: Int) -> WAUrl
    
    func allPaintingsUrl(for artist: WAArtistPage, page: Int) -> WAUrl
    
    func artistPageUrl(for artist: WAArtistInfo) -> WAUrl
    
    func url(for artworksCategory: WAArtworksCategory) -> WAUrl
    
    func url(for artistCategory: WAArtistsCategories) -> WAUrl
    
    func urlSearchForArt(with term:String) -> WAUrl
    
    func urlSearchForArtist(with term: String) -> WAUrl
    
    func artistsList(for category: WAArtistsCategories, item: ArtistsDictionariesWithCategories?, page: Int) -> WAUrl
    
}

extension WAUrlFactory {
    
    func relatedPaintingsUrl(for painting: WAPainting) -> WAUrl {
        return .parts(Current.language, .relatedArtworks(painting.paintingUrl), WAUrlParameters.singleValue(.jsonV2))
    }
    
    func featuredPaintingsUrl(for artist: WAArtistPage) -> WAUrl {
        return .parts( Current.language, .featuredPaintingForArtist( artist.url ?? "" ), .singleValue( .jsonV2 ) )
    }
    
    func artistsList(for category: WAArtistsCategories, item: ArtistsDictionariesWithCategories?, page: Int) -> WAUrl {
         return .parts(Current.language, .artists(category, item), .multipleValue(.jsonV3, .multipleValue( .layout, .multipleVariable(.page(page), .singleVariable( .searchTerm(item?.url ?? "" ))))))
    }
    
    func urlSearchForArt(with term:String) -> WAUrl {
        return .parts(Current.language, .search(term), .multipleVariable(.pageSize(5000), .singleValue(.jsonV2)))
    }
    
    func urlSearchForArtist(with term: String) -> WAUrl {
        return .parts(Current.language, .search(term), .multipleVariable(.pageSize(5000), .multipleValue(.jsonV3, .singleValue(.layout))))
    }
    
    func url(for artistCategory: WAArtistsCategories) -> WAUrl {
        return .parts(Current.language, .artists(artistCategory, nil), .singleValue(.jsonV2))
    }
    
    func url(for artworksCategory: WAArtworksCategory) -> WAUrl {
        return .parts(Current.language, .app(.dictionary), .singleValue( WAArtworksCategory.parameterForCategory[artworksCategory]!) )
    }
    
    func paintings(for category: WAArtworksCategory, item: ArtworksTypeServer?, page: Int) -> WAUrl {
        return makeUrl(forCategory: category, item: item, page: page)
    }
    
    func allPaintingsUrl(for artist: WAArtistPage, page: Int = 1) -> WAUrl {
        return .parts(Current.language, .allPainting(artist.artistEndPoint), .multipleValue(.jsonV2, .singleVariable(.page(page))))
    }

    func relatedArtUrl(for artwork: WAPainting) -> WAUrl {
        return .parts(Current.language, .relatedArtworks(artwork.paintingUrl), .singleValue(.jsonV2))
    }
    
    func artistPageUrl(for artist: WAArtistInfo) -> WAUrl {
        return .parts(Current.language, .artist(artist.artistEndPoint), .singleValue(.jsonV2))
    }
    
}
