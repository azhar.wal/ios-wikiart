//
//  FavoritesManager.swift
//  WikiArt2.0
//
//  Created by Waleed Azhar on 2018-09-13.
//  Copyright © 2018 Waleed Azhar. All rights reserved.
//

import Foundation

final class Favorites {
    
    static var paintingsKey = "fav_paintings"
    static var artistsKey = "fav_paintings"
    
    private var artists: Set<WAArtistPage> = []
    private var paintings: Set<WAPainting> = []
 
}

extension Favorites {
    
    public func contains(_ a: WAArtistPage) -> Bool {
        return artists.contains(a)
    }
    
    public func contains(_ a: WAPainting) -> Bool {
        return paintings.contains(a)
    }
    
    public func append(artist a: WAArtistPage) {
        artists.insert(a)
        
    }
    
    public func remove(artist a: WAArtistPage) {
        artists.remove(a)
    }
    
    public func append(painting p: WAPainting) {
        if !paintings.contains(p) {
            paintings.insert(p)
            self.save()
        }
    }
    
    public func remove(painting p: WAPainting) {
        paintings.remove(p)
        self.save()
    }
    
}

extension Favorites {
    
    public var artistsSorted: [ WAArtistPage ] {
        return artists.sorted() { (lhs, rhs) -> Bool in
            if let a = lhs.artistNamed, let b = rhs.artistNamed {
                return a.caseInsensitiveCompare(b) == ComparisonResult.orderedAscending
            }
            return false
        }
    }
    
    public var paintingsSorted: [ WAPainting ] {
        return paintings.sorted() { (a, b) -> Bool in
            return a.titled.caseInsensitiveCompare( b.titled )  == ComparisonResult.orderedAscending
        }
    }
    
}

extension Favorites {
    
    public func save() {
        
        let encoder = JSONEncoder()
        if let data = try? encoder.encode(paintingsSorted) {
            UserDefaults.standard.set(data, forKey: Favorites.paintingsKey )
        }
        
    }
    
    public func restore() {
        
        let decoder = JSONDecoder()
        guard let data = UserDefaults.standard.value(forKey: Favorites.paintingsKey) as? Data else { return }
        if let ps = try? decoder.decode([WAPainting].self, from: data) { paintings = paintings.union(ps) }
    
    }
    
}
