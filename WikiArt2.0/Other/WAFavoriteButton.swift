//
//  WAFavoriteButton.swift
//  WikiArt2.0
//
//  Created by Waleed Azhar on 2018-09-13.
//  Copyright © 2018 Waleed Azhar. All rights reserved.
//

import UIKit

class WAFavoriteButton: UIButton {

    public func configure( faved : Bool ) {
        let faveText = faved ? "Favorited" : "Favorite"
        tintColor = faved ? .darkGray : .red
        setTitle(faveText, for: .normal)
    }
    
}
