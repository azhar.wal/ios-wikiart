//
//  UIViewExtensions.swift
//  WikiArt2.0
//
//  Created by Waleed Azhar on 2017-09-24.
//  Copyright © 2017 Waleed Azhar. All rights reserved.
//

import UIKit

extension UINib {
    
    struct NibName {
        static let ArtistTableViewCell = "WAArtistTableViewCell"
    }
    
    static var artistTableViewCell: UINib {
        return UINib.init(nibName: NibName.ArtistTableViewCell, bundle: nil)
    }

}

extension UITabBarItem {
    
    enum WikiArtTabBarItem: String {
        case artworks = "Paintings"
        case artists = "Artists"
        case search = "Search"
    }
    
    convenience init(wikiArtItem: WikiArtTabBarItem, tag: Int) {
        self.init(title: wikiArtItem.rawValue, image: UIImage(named:wikiArtItem.rawValue), tag: tag)
    }
}
extension UIViewController {
    
    static func appRootViewController() -> UITabBarController {
     
        let nvc = UINavigationController(root: WAPaintingCollectionViewController())
        nvc.tabBarItem = UITabBarItem(wikiArtItem: .artworks, tag: 1)
        
        let nvc2 = UINavigationController(root: WAArtistsTableViewController() )
        nvc2.tabBarItem = UITabBarItem(wikiArtItem: .artists, tag: 2)
        
        let nvc3 = UINavigationController(root: WASearchViewController() )
        nvc3.tabBarItem = UITabBarItem(wikiArtItem: .search, tag: 3)
        
        return UITabBarController(with: [ nvc, nvc2, nvc3 ] )
    }
    
}

extension UITabBarController {
    
    convenience init(with vcs:[UIViewController] ) {
        self.init()
        setViewControllers(vcs, animated: false)
        self.tabBar.isTranslucent = true
        self.tabBar.tintColor = .black
    }
    
}

extension UINavigationController {
    
    convenience init(root: UIViewController) {
        self.init(rootViewController: root)
        self.navigationBar.tintColor = .black
        self.navigationBar.isTranslucent = true
    }
    
}

extension UIView {
    static func springAnimation(duration: TimeInterval, animation: @escaping () -> Void, completion: ((Bool) -> Void)?) {
        UIView.animate(withDuration: duration, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 5, options: .beginFromCurrentState, animations: animation, completion: completion)
    }
}


extension UIView {
    
    enum WAViewEdge {
        case Top, Bottom, Left, Right, None
    }
    
    enum WAViewDimension {
        case Width, Height
    }
    
    func pinToEdges(view: UIView, excludingEdge: WAViewEdge) {
        let left = view.leftAnchor.constraint(equalTo: self.leftAnchor)
        let right = view.rightAnchor.constraint(equalTo: self.rightAnchor)
        let bottom = view.bottomAnchor.constraint(equalTo: self.bottomAnchor)
        let top = view.topAnchor.constraint(equalTo: self.topAnchor)
        var constraints: [NSLayoutConstraint] = []
        
        switch excludingEdge {
        case .Top:
            constraints = [left, right, bottom]
        case .Bottom:
            constraints = [left, right, top]
        case .Left:
            constraints = [right, top, bottom]
        case .Right:
            constraints = [left, top, bottom]
        case .None:
            constraints = [left, right, top, bottom]
        }
        
        NSLayoutConstraint.activate(constraints)
    }
    
    func constraint(view: UIView , dimension: WAViewDimension ) {
        switch dimension {
        case .Width:
            view.widthAnchor.constraint(equalTo: self.widthAnchor).isActive = true
            
        case .Height:
            view.heightAnchor.constraint(equalTo: self.heightAnchor).isActive = true
        }
    }
}

extension UIViewController {

    func embedInNavigationController() -> UINavigationController {
        return UINavigationController(root: self)
    }
    
}

extension UIViewController {
    
    public struct AssociatedKeys {
        static var Message = "Message"
        static var ActivityIndicator = "UIActivityIndicatorView"
    }
    
    var activityIndicator: UIActivityIndicatorView {
        get {
            if let actInd = objc_getAssociatedObject(self, &AssociatedKeys.ActivityIndicator) as? UIActivityIndicatorView {
                return actInd
            } else {
                let actInd = UIActivityIndicatorView( style: .gray )
                self.view.addSubview(actInd)
                actInd.translatesAutoresizingMaskIntoConstraints = false
                actInd.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
                actInd.centerYAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.centerYAnchor).isActive = true
                self.activityIndicator = actInd
                return actInd
            }
        }
        
        set (value) {
            objc_setAssociatedObject(self, &AssociatedKeys.ActivityIndicator, value, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
        
    }
    
    var messageLabel: UILabel {
        get {
            if let label = objc_getAssociatedObject(self, &AssociatedKeys.Message) as? UILabel {
                return label
            } else {
                let label = UILabel()
                self.view.addSubview(label)
                label.translatesAutoresizingMaskIntoConstraints = false
                label.font = UIFont.boldSystemFont(ofSize: 22)
                label.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
                label.centerYAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.centerYAnchor).isActive = true
                self.messageLabel = label
                return label
            }
        }
        
        set (value) {
            objc_setAssociatedObject(self, &AssociatedKeys.Message, value, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
        
    }
    
}

extension UIImageView {
    
    public struct AssociatedKeys {
        static var Task = "task"
        static var activityIndicator = "UIActivityIndicatorView"
    }
    
    var activityIndicator: UIActivityIndicatorView {
        get {
            if let actInd = objc_getAssociatedObject(self, &AssociatedKeys.activityIndicator) as? UIActivityIndicatorView {
                return actInd
            } else {
                let actInd = UIActivityIndicatorView( style: .gray )
                self.addSubview(actInd)
                actInd.translatesAutoresizingMaskIntoConstraints = false
                actInd.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
                actInd.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
                self.activityIndicator = actInd
                return actInd
            }
        }
        
        set (value) {
            objc_setAssociatedObject(self, &AssociatedKeys.activityIndicator, value, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
        
    }
    
    var task: URLSessionDataTask? {
        
        get { return objc_getAssociatedObject(self, &AssociatedKeys.Task) as? URLSessionDataTask }
        set (value) { objc_setAssociatedObject(self, &AssociatedKeys.Task, value, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC) }
        
    }
    
    func loadURL( image: String? ) {
        if let imageURl = image {
            loadURL(image: imageURl)
        }
    }
    
    func loadURL( image: String ) {
        activityIndicator.startAnimating()
        if let task = self.task { task.cancel() }

        if let url = NSURL.init(string: image) {
            
            if let image = imageCache.object(forKey: url) {
                self.image = image
                activityIndicator.stopAnimating()
                return
            }
            
            
            let imageData = Resource<UIImage>(url: url)
            let request = NSMutableURLRequest(resource: imageData)
            
            self.task = URLSession.shared.dataTask(with: request as URLRequest) { (data, response, _) in

                guard let data = data else { return }
                
                if let image = UIImage(data: data) {
    
                    imageCache.setObject(image, forKey: response!.url! as NSURL)
                    
                    DispatchQueue.main.async { [weak self] in
                        self?.alpha = 0
                        self?.image = image
                        self?.activityIndicator.stopAnimating()
                        UIView.animate(withDuration: 0.2) { self?.alpha = 1 }
                    }
                }
            }
            
            self.task?.resume()
        }
        
    }
    
    func loadPainting( painting: WAPainting, size: WAPaintingSize) {
       loadURL(image: WAPaintingSize.change(artwork: painting.image, to: size))
    }
    
}

extension UIFont {
 
}

extension UICollectionView {
    func reloadItems(fromRow: Int, toRow:Int ) {
    
        var indexPaths:[IndexPath] = []
        for i in fromRow..<toRow {
            indexPaths.append(IndexPath(row: i, section: 0))
        }
        self.reloadItems(at: indexPaths)
    }
}
