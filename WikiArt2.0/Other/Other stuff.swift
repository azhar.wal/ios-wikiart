//
//  Other stuff.swift
//  Cells
//
//  Created by Waleed Azhar on 9/28/17.
//  Copyright © 2017 Waleed Azhar. All rights reserved.
//

import Foundation

extension Int {
    var number: NSNumber {
        return NSNumber(integerLiteral: self)
    }
    
    var double: Double {
        return Double(self)
    }
}

extension String {
    
    var convertFromHTMLUnicode: String {
        var myString = self
        
        let convertDict:[String:String] = ["&#39;" : "ú",
                                           "&#224;" : "à",
                                           "&#237;" : "í",
                                           "&amp;" : "&",
                                           "&#160;" : " ",
                                           "&#233;" : "é",
                                           "&#246;" : "ö",
                                           "&#240;" : "ð",
                                           "&#225;" : "á",
                                           "&#231;" : "ç",
                                           "&#227;" : "ã",
                                           "&#244;" : "ô",
                                           "&#251;" : "û",
                                           "&#186;" : "º",
                                           "&#234;" : "ê",
                                           "&#232;" : "è",
                                           "&quot;" : "\"",
                                           "&#226;" : "â",
                                           "&#201;" : "É",
                                           "&#243;" : "ó",
                                           "&#250;" : "ú",
                                           "&#252;" : "ü",
                                           "&#228;" : "ä"]
        
        for (key,value) in convertDict {
            myString = myString.replacingOccurrences(of: key, with: value)
            
        }
        
        return myString
    }
    
}


extension Array where Array.Element == String? {
    
    public func reduce(_ separator: String = " | " ) -> String {
        var result = ""
        
        for itemOption in self {
            if let item = itemOption {
                result += item + separator
            }
        }
        
        return String(result.dropLast(separator.count))
    }
    
}
