//
//  WAArtistCollectionViewController.swift
//  WikiArt2.0
//
//  Created by Waleed Azhar on 2017-09-24.
//  Copyright © 2017 Waleed Azhar. All rights reserved.
//

import UIKit
import StoreKit

class WAArtistsTableViewController: UITableViewController, UIViewControllerTransitioningDelegate {
    
    var artistDataSource: WAArtistsDataSource? = nil
    
    var selectedCategory = WAArtistsCategories.popular
    
    var selectedItem: ArtistsDictionariesWithCategories? = nil
    
    func loadList() {
        
        artistDataSource = nil
        tableView.reloadData()
        
        activityIndicator.startAnimating()
        artistDataSource = WAArtistsDataSource(category: selectedCategory, item: selectedItem)
        if let item = selectedItem { navigationItem.title = item.Title } else { navigationItem.title = selectedCategory.title() }
        
        artistDataSource!.completion = { [weak self] _ in
            guard let this = self else { return }
            this.activityIndicator.stopAnimating()
            this.messageLabel.text = nil
            this.tableView.reloadData()
        }
        
        artistDataSource!.error = { [weak self] in
            self?.activityIndicator.stopAnimating()
            self?.messageLabel.text = "An Error Occured"
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.separatorStyle = .none
        tableView.register( UINib.artistTableViewCell, forCellReuseIdentifier: UINib.NibName.ArtistTableViewCell)
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Categories", style: .plain, target: self, action: #selector(pressed(_:)))
        
        loadList()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if (arc4random_uniform(15) == 1) {
            SKStoreReviewController.requestReview()
        }
        
        navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    @objc func pressed(_ sender: Any) {
        
        let vc = WAArtistOptionsTableViewController()
        vc.delegate = self
        
        let nav = UINavigationController(root: vc)
        nav.modalPresentationStyle = .custom
        nav.transitioningDelegate = self
        
        present(nav, animated: true, completion: nil)
    }

    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        return WAMenuPresentationController(presentedViewController: presented, presenting: presenting)
    }

}

extension WAArtistsTableViewController {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return artistDataSource?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: UINib.NibName.ArtistTableViewCell, for: indexPath) as! WAArtistTableViewCell
        
        if let artist = artistDataSource?.artists?.Artists[indexPath.row] {
            cell.configure(forArtist: artist)
            cell.accessoryType = .disclosureIndicator
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let artist = artistDataSource?.artists?.Artists[indexPath.row] else { return }
        
        //TODO: Clean up
        let bioUrl: WAUrl = .parts(Current.language, WAUrlLocation.artist(artist.artistEndPoint), WAUrlParameters.singleValue(.jsonV2))

        let loadingVC = WALodingResourceViewController(resource: Resource<WAArtistPage>.init(url: bioUrl)) { artistPage in
            let vc = UIStoryboard.init(name: "WAArtistDetails", bundle: nil).instantiateInitialViewController() as! WAArtistDetailsTableViewController
            vc.artistPage = artistPage
            return vc
            
        }
        
        navigationController?.navigationBar.prefersLargeTitles = false
        navigationController?.pushViewController(loadingVC, animated: true)
    }
    
}

extension WAArtistsTableViewController: WAArtistOptionsTableViewDelegate {
   
    func selected(artistCategory: WAArtistsCategories, item: ArtistsDictionariesWithCategories?) {
        selectedCategory = artistCategory
        selectedItem = item
        loadList()
        presentedViewController?.dismiss(animated: true, completion: nil)
    }
    
}
