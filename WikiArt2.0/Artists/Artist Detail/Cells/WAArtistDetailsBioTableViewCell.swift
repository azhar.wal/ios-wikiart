//
//  WAArtistDetailsBioTableViewCell.swift
//  WikiArt2.0
//
//  Created by Waleed Azhar on 2018-10-10.
//  Copyright © 2018 Waleed Azhar. All rights reserved.
//

import UIKit

class WAArtistDetailsBioTableViewCell: UITableViewCell {

    @IBOutlet weak var bio: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
