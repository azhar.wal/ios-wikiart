//
//  WAArtistDetailsBioTableViewCell.swift
//  WikiArt2.0
//
//  Created by Waleed Azhar on 2018-10-10.
//  Copyright © 2018 Waleed Azhar. All rights reserved.
//

import UIKit

class WAArtistDetailsPopularArtTableViewCell: UITableViewCell {

    var paintingList: WAPaintingsList?
    var didSelect: (IndexPath, WAPaintingsList?) -> () = { _,_  in }

    @IBOutlet weak var collectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(WAPaintingCollectionViewCell.self, forCellWithReuseIdentifier: WAPaintingCollectionViewCell.description())
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}

extension WAArtistDetailsPopularArtTableViewCell: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        didSelect(indexPath, paintingList)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height: CGFloat = 240
        let maxWidth: CGFloat = collectionView.frame.width
        
        if let painting = paintingList?.Paintings[indexPath.row] {
            return CGSize(width:min(height*painting.width/painting.height, maxWidth), height: WAPaintingCollectionViewCell.UnderHangHeight + height)
        }
        return CGSize(width: height, height: maxWidth)
    }
    
}

extension WAArtistDetailsPopularArtTableViewCell: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return paintingList?.Paintings.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: WAPaintingCollectionViewCell.description(), for: indexPath) as! WAPaintingCollectionViewCell
        
        if let painting = paintingList?.Paintings[indexPath.row] {
            cell.configure(forPainting: painting, paintingSize: .portrait)
        }
        
        return cell
    }
    
    
}
