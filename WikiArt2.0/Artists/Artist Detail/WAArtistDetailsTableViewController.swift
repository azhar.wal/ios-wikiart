//
//  WAArtistDetailsTableViewController.swift
//  WikiArt2.0
//
//  Created by Waleed Azhar on 2018-10-10.
//  Copyright © 2018 Waleed Azhar. All rights reserved.
//

import UIKit

class WAArtistDetailsTableViewController: UITableViewController {
    
    enum ReuseIDS: String {
        case bio = "bio"
        case popular = "popular"
        case artist = "artist"
        case seeAll = "seeAll"
    }
    
    private let cellOrder:[ReuseIDS] = [.artist, .bio, .popular, .seeAll ]
    
    @IBOutlet weak var imageView: UIImageView!
    
    var artistPage: WAArtistPage?
    
    private var paintings: WAPaintingsList?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Artist"

        imageView.loadURL(image: artistPage?.image)
        guard let artist = artistPage else { return }
        Current.service.getFeaturedPaintings(for: artist) { [weak self] list in
            self?.paintings = list
            self?.tableView.reloadSections(IndexSet.init(integer: 2), with: UITableView.RowAnimation.none)
        }
        
    }
    
    @IBAction func seeAll(_ sender: Any) {
        
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return cellOrder.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            return 1
        }
        else if section == 1, let bio = artistPage?.biography, bio.count > 10 {
            return 1
        }
        else if section == 2, let list = paintings?.Paintings, list.count > 0 {
            return 1
        }
        else if section == 3 {
            return 1
        }
    
        
        return 0
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let reuseID = cellOrder[indexPath.section]
        
        switch reuseID {
        case .artist:
            let cell = tableView.dequeueReusableCell(withIdentifier: reuseID.rawValue, for: indexPath) as! WAArtistDetailsArtistTableViewCell
            cell.title.text = artistPage?.artistNamed
            cell.detail.text = artistPage?.details
            
            return cell
        case .bio:
            let cell = tableView.dequeueReusableCell(withIdentifier: reuseID.rawValue, for: indexPath) as! WAArtistDetailsBioTableViewCell
            cell.bio.text = artistPage?.biography
            cell.accessoryType = .disclosureIndicator
            return cell
        
        case .popular:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: reuseID.rawValue, for: indexPath) as! WAArtistDetailsPopularArtTableViewCell
            cell.paintingList = paintings
            
            cell.didSelect = { [weak self] indexPath, paintingList in
                if let painting = paintingList?.Paintings[indexPath.row] {
                    self?.navigationController?.pushViewController(WAArtworkDetailsViewController(artwork: painting), animated: true)
                }
            }
            
            cell.collectionView.reloadData()
            return cell
        
        case .seeAll:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: reuseID.rawValue, for: indexPath)
        
            cell.accessoryType = .disclosureIndicator

            return cell


        }
        
    }
    

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        if indexPath.section == 1 {
            let vc = WATextfieldViewController(heading: "Biography", biography: artistPage?.biography)
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if indexPath.section == 3, let artist = artistPage {
            let vc = WAPaintingCollectionViewController(artist: artist)
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
    }
   
}
