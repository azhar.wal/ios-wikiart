//
//  WAArtistCategoriesTableViewController.swift
//  WikiArt2.0
//
//  Created by Waleed Azhar on 2018-10-09.
//  Copyright © 2018 Waleed Azhar. All rights reserved.
//

import UIKit

protocol WAArtistOptionsTableViewDelegate {
    func selected(artistCategory: WAArtistsCategories, item: ArtistsDictionariesWithCategories?)
}

class WAArtistOptionsTableViewController: UITableViewController {

    public var delegate: WAArtistOptionsTableViewDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Categories"
        tableView.register(WAMenuViewMenuItemTableViewCell.self, forCellReuseIdentifier: WAMenuViewMenuItemTableViewCell.description() )
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancel))
    }
    
    @objc func cancel() {
        presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ArtistsCategoriesDataSource.artistsCategories.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: WAMenuViewMenuItemTableViewCell.description(), for: indexPath)
        
        let category = ArtistsCategoriesDataSource.artistsCategories[indexPath.row]
        cell.textLabel?.text = category.title().capitalized
        cell.accessoryType = ArtistsCategoriesDataSource.categoriesWithTypes.contains(category) ? .disclosureIndicator : .none

        return cell
    }
    
    public override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let category = ArtistsCategoriesDataSource.artistsCategories[indexPath.row]
        
        if ArtistsCategoriesDataSource.categoriesWithTypes.contains(category) {
            let tvc = WAOptionDetailTableViewController( artistCategory: category )
            tvc.title = category.title()
            tvc.delegate = self
            self.navigationController?.pushViewController(tvc, animated: true)
            
        } else { delegate?.selected(artistCategory: category, item: nil) }

    }
    
    
}

extension WAArtistOptionsTableViewController: WAOptionDetailTableViewDelegate {
    
    func selected(artworkCategory: WAArtworksCategory, item: ArtworksTypeServer) {
       
    }
    
    func selected(artistCategory: WAArtistsCategories, item: ArtistsDictionariesWithCategories) {
        delegate?.selected(artistCategory: artistCategory, item: item)
    }
    
}
