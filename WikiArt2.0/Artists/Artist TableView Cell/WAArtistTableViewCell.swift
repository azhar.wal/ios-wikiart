//
//  WAArtistTableViewCell.swift
//  WikiArt2.0
//
//  Created by Waleed Azhar on 2018-10-09.
//  Copyright © 2018 Waleed Azhar. All rights reserved.
//

import UIKit

class WAArtistTableViewCell: UITableViewCell {
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subtitle: UILabel!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        profileImageView.image = nil
        title.text = nil
        subtitle.text = nil
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configure(forArtist artist: WAArtistInfo) {
        profileImageView?.loadURL(image: artist.image)
        title?.text = artist.title
        subtitle.text = [artist.nation, artist.totalWorksTitle, artist.year].reduce()
    }
    
}
