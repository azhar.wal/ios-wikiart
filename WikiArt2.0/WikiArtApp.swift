//
//  AppState.swift
//  WikiArt2.0
//
//  Created by Waleed Azhar on 2018-04-15.
//  Copyright © 2018 Waleed Azhar. All rights reserved.
//

import Foundation
import UIKit

final class WikiArtApp {
    
    static var Current: WikiArtApp!
    
    let language: WALanguage
    
    let service: ServiceType
    
    var favorites = Favorites()
    
    convenience init( languageCode: String? ) {
        self.init(language: WALanguage(rawValue: languageCode ?? "" ) ?? .en )
    }
    
    init( language: WALanguage ) {
        self.language = language
        self.service = WikiArtService( language: language, resourceFactory: WikiArtResources(), webService: WebService() )
        self.favorites.restore()
    }
    
}
