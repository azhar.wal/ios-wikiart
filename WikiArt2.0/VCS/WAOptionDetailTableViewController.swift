//
//  WAOptionDetailTableViewController.swift
//  WikiArt2.0
//
//  Created by Waleed Azhar on 2018-10-16.
//  Copyright © 2018 Waleed Azhar. All rights reserved.
//

import UIKit

protocol WAOptionDetailTableViewDelegate {
    func selected(artworkCategory: WAArtworksCategory, item: ArtworksTypeServer)
    func selected(artistCategory: WAArtistsCategories, item: ArtistsDictionariesWithCategories)
}

final class WAOptionDetailTableViewController: UITableViewController {
    
    enum WAOptionDetailTableViewControllerType {
        case artwork
        case artist
    }
    
    private let type: WAOptionDetailTableViewControllerType
    
    private var artworkCategory: WAArtworksCategory!
    
    private var artistCategory: WAArtistsCategories!
    
    private var artworkItems :[ArtworksTypeServer]!
    
    private var artistItems: [ArtistsDictionariesWithCategories]!
    
    public var delegate:WAOptionDetailTableViewDelegate?
    
    init(artworkCategory: WAArtworksCategory ) {
        self.artworkCategory = artworkCategory
        self.type = .artwork
        super.init(style: .plain)
    }
    
    init(artistCategory: WAArtistsCategories ) {
        self.artistCategory = artistCategory
        self.type = .artist
        super.init(style: .plain)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func loadContent() {
       
        switch type {
            case .artwork:
                
                Current.service.getCategories(for: artworkCategory!) { [weak self] items in
                    guard let items = items else { return }
                    self?.artworkItems = items
                    self?.activityIndicator.stopAnimating()
                    self?.tableView.reloadData()
                }
            
            case .artist:
                
                Current.service.getCategories(for: artistCategory! ) { [weak self] artistTypes in
                    guard let artistTypes = artistTypes else { return }
                    self?.artistItems = artistTypes.items
                    self?.activityIndicator.stopAnimating()
                    self?.tableView.reloadData()
                }
        }
    }
    
}

extension WAOptionDetailTableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register( WAMenuViewMenuItemTableViewCell.self, forCellReuseIdentifier: WAMenuViewMenuItemTableViewCell.description() )
        activityIndicator.startAnimating()
        loadContent()
    
    }
}

extension WAOptionDetailTableViewController {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch type {
            case .artwork: return artworkItems?.count ?? 0
            case .artist: return artistItems?.count ?? 0
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: WAMenuViewMenuItemTableViewCell.description(), for: indexPath)
        
        switch type {
            case .artwork: cell.textLabel?.text = artworkItems[indexPath.row].title
            case .artist: cell.textLabel?.text = artistItems[indexPath.row].Title.capitalized
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch type {
            case .artwork: delegate?.selected( artworkCategory: artworkCategory, item: artworkItems[indexPath.row] )
            case .artist: delegate?.selected( artistCategory: artistCategory, item: artistItems[indexPath.row] )
        }
    }
    
}
