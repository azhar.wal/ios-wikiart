
//
//  WABottomLoadindIndicatorViewController.swift
//  Mangazine
//
//  Created by Waleed Azhar on 10/23/17.
//  Copyright © 2017 Waleed Azhar. All rights reserved.
//

import UIKit

extension UIView {
    class func springAnimation(withDuration duration: TimeInterval = 1,
                               delay: TimeInterval = 0,
                               options: UIView.AnimationOptions = [UIView.AnimationOptions.curveEaseInOut, UIView.AnimationOptions.beginFromCurrentState, UIView.AnimationOptions.allowUserInteraction],
                               animations: @escaping () -> Swift.Void,
                               completion: ((Bool) -> Swift.Void)? = nil) {
        UIView.animate(withDuration: duration, delay: delay, usingSpringWithDamping: 0.5, initialSpringVelocity:1, options: options, animations: animations, completion: completion)
    }
}

@objc
protocol WALoadingIndicatorCollectionViewControllerDataSource: UICollectionViewDataSource {

}

@objc
protocol WALoadingIndicatorCollectionViewControllerDelegate: UICollectionViewDelegate {
    func loadingIndicatorCollectionViewShouldShowLoadingIndicator() -> Bool
}

class WALoadingIndicatorCollectionViewController: UIViewController, UIScrollViewDelegate, WALoadingIndicatorCollectionViewControllerDataSource, WALoadingIndicatorCollectionViewControllerDelegate {
    
    public weak var dataSource: WALoadingIndicatorCollectionViewControllerDataSource?
    
    public weak var delegate: WALoadingIndicatorCollectionViewControllerDelegate?
    
    public var loadingString = "Loading More"
    
    public var completeString = "All Content Loaded"
    
    public var collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    
    private var loadingContainerView = UIView()
    
    private var loadingIndicatorView = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.white)
    
    private var loadingLabelView = UILabel()
    
    private var loadingContainerBottomConstraint = NSLayoutConstraint()
    
    private let loadingContainerBottomVisible: CGFloat = -10
    
    private let loadingContainerBottomHidden: CGFloat = 88
    
    private var stackView = UIStackView()
    
    private var isVisible = false
    
    private var loadingViewVisible: Bool {
        return loadingContainerBottomConstraint.constant == loadingContainerBottomVisible
    }
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
        setupConstraints()
    }
    
    private func setupSubviews() {
        
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        loadingContainerView.translatesAutoresizingMaskIntoConstraints = false
        loadingIndicatorView.translatesAutoresizingMaskIntoConstraints = false
        loadingLabelView.translatesAutoresizingMaskIntoConstraints = false
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        view.backgroundColor = .white
        view.addSubview(collectionView)
        view.addSubview(loadingContainerView)
        
        stackView.addArrangedSubview(loadingIndicatorView)
        stackView.addArrangedSubview(loadingLabelView)
        stackView.spacing = 8.0
        loadingContainerView.addSubview(stackView)
        
        loadingContainerView.backgroundColor = .black
        loadingContainerView.layer.cornerRadius = 14
        loadingIndicatorView.startAnimating()
        
        loadingLabelView.text = loadingString
        loadingLabelView.textColor = .white
        loadingLabelView.font = UIFont.systemFont(ofSize: 12.0)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(tap(t:)))
        loadingContainerView.addGestureRecognizer(tap)
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.backgroundColor = .white
        collectionView.clipsToBounds = false
        
        dataSource = self
        delegate = self
    
    }
    
    private func setupConstraints() {
        
        collectionView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        collectionView.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor).isActive = true
        collectionView.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor).isActive = true
        collectionView.bottomAnchor.constraint(equalTo:  view.safeAreaLayoutGuide.bottomAnchor).isActive = true

        loadingContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        loadingContainerView.widthAnchor.constraint(equalToConstant: 140).isActive = true
        loadingContainerBottomConstraint = loadingContainerView.bottomAnchor.constraint(equalTo: collectionView.bottomAnchor, constant: loadingContainerBottomHidden)
        loadingContainerBottomConstraint.isActive = true
        loadingContainerView.heightAnchor.constraint(equalToConstant: 28).isActive = true
        
        
        loadingIndicatorView.alpha = 0
        loadingLabelView.alpha = 0
        
        stackView.centerXAnchor.constraint(equalTo: loadingContainerView.centerXAnchor).isActive = true
        stackView.centerYAnchor.constraint(equalTo: loadingContainerView.centerYAnchor).isActive = true
    }
    
}

extension WALoadingIndicatorCollectionViewController {
    
    public func toggleLoadingView() {
        if loadingViewVisible {
            hideLoading()
        }
        else {
            showLoading()
        }
    }
    
    @objc func tap(t:UITapGestureRecognizer) {
        //toggleLoadingView()
    }
    
    private func showLoading() {
        self.view.layoutIfNeeded()
        loadingContainerBottomConstraint.constant = loadingContainerBottomVisible
        isVisible = true

        UIView.springAnimation(animations: {
            self.loadingIndicatorView.alpha = 1
            self.loadingLabelView.alpha = 1
            self.view.layoutIfNeeded()
        })
        
    }
    
    func hideLoading() {
       self.view.layoutIfNeeded()
        loadingContainerBottomConstraint.constant = loadingContainerBottomHidden

        UIView.springAnimation(animations: {
            self.view.layoutIfNeeded()
            self.loadingLabelView.alpha = 0
            self.loadingIndicatorView.alpha = 0
        }, completion: { _ in self.isVisible = false
            self.view.layoutIfNeeded()
        })
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        // TODO: effiency
        if Int(scrollView.contentSize.height) - Int((scrollView.contentOffset.y + scrollView.frame.height)) < 4 {
            if let d = self.delegate {
                if isVisible == false && d.loadingIndicatorCollectionViewShouldShowLoadingIndicator() {
                    toggleLoadingView()
                }
            }
        }
    }
    
}

extension WALoadingIndicatorCollectionViewController {

    func loadingIndicatorCollectionViewShouldShowLoadingIndicator() -> Bool {
        return false
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return dataSource?.numberOfSections!(in: collectionView) ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource?.collectionView(collectionView, numberOfItemsInSection: section) ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return dataSource!.collectionView(collectionView, cellForItemAt:indexPath)
    }
    
}


