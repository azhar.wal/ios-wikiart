//
//  WAMenuViewController.swift
//  WikiArt2.0
//
//  Created by Waleed Azhar on 2017-10-03.
//  Copyright © 2017 Waleed Azhar. All rights reserved.
//
import UIKit

final class WAMenuViewMenuItemTableViewCell: UITableViewCell {

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.textLabel?.font = UIFont.systemFont(ofSize: 14, weight: .medium)
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        UIView.springAnimation(duration: 0.1, animation: {
            self.backgroundColor = selected ? UIColor.black : UIColor.white
            self.textLabel?.textColor = selected ? .white : .darkText
        }, completion: nil)
    }
    
}


