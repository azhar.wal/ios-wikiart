//
//  WAMenuPresentationController.swift
//  WikiArt2.0
//
//  Created by Waleed Azhar on 2017-10-03.
//  Copyright © 2017 Waleed Azhar. All rights reserved.
//

import UIKit

class WAMenuPresentationController: UIPresentationController {
    
    public var heightPercent: CGFloat = 0.4

    let shadow = UIView()
    
    override var frameOfPresentedViewInContainerView: CGRect {
        let containerBounds = (containerView?.bounds)!
        return containerBounds.insetBy(dx: 40, dy: 40)
    }
    
    override func presentationTransitionWillBegin() {
        shadow.frame = containerView!.frame
        shadow.backgroundColor = UIColor.init(white: 0, alpha: 0)
        containerView?.addSubview(shadow)
        shadow.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismiss)))
        UIView.animate(withDuration: 0.4) {
            self.shadow.backgroundColor = UIColor.init(white: 0.2, alpha: 0.8)
        }
    }
    
    @objc func dismiss() {
        presentingViewController.dismiss(animated: true, completion: nil)
    }
    
    override func dismissalTransitionWillBegin() {
        UIView.animate(withDuration: 0.4) {
            self.shadow.backgroundColor = UIColor.init(white: 1, alpha: 0)
        }
    }
}

