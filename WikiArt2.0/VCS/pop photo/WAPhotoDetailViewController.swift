//
//  WAPhotoDetailViewController.swift
//  Cells
//
//  Created by Waleed Azhar on 9/27/17.
//  Copyright © 2017 Waleed Azhar. All rights reserved.
//

import UIKit

class WAPhotoDetailViewController: UIViewController, UIScrollViewDelegate, WAPaintingAnimatorDataSource {

    private var closeButton = UIButton(type: UIButton.ButtonType.system) /// TODO: update
    
    private var scrollView = UIScrollView()
    
    private var imageView = UIImageView()
    
    private var centerX: NSLayoutConstraint?
    
    public var image: UIImage

    init(image: UIImage) {
        self.image = image
        super.init(nibName: nil, bundle: nil)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        scrollView.delegate = self
        scrollView.maximumZoomScale = 2.0
        scrollView.minimumZoomScale = 1.0
      
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func setupConstraints() {
        scrollView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        scrollView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        scrollView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        
        imageView.heightAnchor.constraint(equalTo: view!.heightAnchor).isActive = true
        
        imageView.widthAnchor.constraint(equalTo: imageView.heightAnchor, multiplier: (image.size.width/image.size.height)).isActive = true
        
//        centerX = imageView.centerXAnchor.constraint(equalToSystemSpacingAfter: view.centerXAnchor, multiplier: 1)
//        centerX?.isActive = true
        
        closeButton.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 10).isActive = true
        closeButton.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 20).isActive = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageView.image = image
        scrollView.addSubview(imageView)
        
        view.addSubview(scrollView)
        closeButton.translatesAutoresizingMaskIntoConstraints = false
        closeButton.addTarget(self, action: #selector(closePressed), for: .touchUpInside)
        closeButton.setImage(UIImage.init(named: "close"), for: .normal)
        closeButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 4, bottom: 0, right: 0)
        closeButton.titleLabel?.font = UIFont.systemFont(ofSize: 12, weight: .medium)
        closeButton.setTitle("Close  ", for: .normal)
        closeButton.tintColor = .white
        closeButton.layer.shadowColor = UIColor.black.cgColor
        closeButton.layer.shadowOpacity = 0.20
        closeButton.layer.shadowOffset = CGSize(width: 0, height: 4)
        
        view.addSubview(closeButton)
        
        UIView.animate(withDuration: 0.4) {
            self.view.backgroundColor = .black
        }
        
        setupConstraints()
        closeButton.sizeToFit()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if imageView.frame.width >= view.frame.width {
            let offset = CGPoint(x: (imageView.frame.width/2) - (view.frame.width/2), y: 0)
            scrollView.setContentOffset(offset, animated: false)
            scrollView.contentSize = imageView.frame.size
        }
        else {
            imageView.center = view.center
        }

    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
    
    func scrollViewShouldScrollToTop(_ scrollView: UIScrollView) -> Bool {
        return false
    }
    
    @objc func closePressed() {
        dismiss(animated: true, completion: nil)
    }
}

extension WAPhotoDetailViewController {
    func imageOfViewToAnimateOpen() -> UIImage {
        return self.image
    }
    
    func frameOfViewToAnimateOpen() -> CGRect {
        return self.imageView.layer.presentation()?.frame ?? .zero
    }
    
    func contentOffSetOfViewToAnimateOpen() -> CGPoint {
        return scrollView.contentOffset
    }
    
    func willBeginToAnimateOpen() {
        self.imageView.isHidden = false
    }
    
    func willEndToAnimateOpen() {
        self.imageView.isHidden = true
    }
    
    func imageOfViewToAnimateClose() -> UIImage {
        return self.image
    }
    
    func frameOfViewToAnimateClose() -> CGRect {
        return self.imageView.layer.presentation()?.frame ?? .zero
    }
    
    func contentOffSetOfViewToAnimateClose() -> CGPoint {
        return scrollView.contentOffset
    }
    
    func willBeginToAnimateClose() {
        self.imageView.isHidden = true
    }
    
    func willEndToAnimateClose() {
        self.imageView.isHidden = false
    }
    
}
