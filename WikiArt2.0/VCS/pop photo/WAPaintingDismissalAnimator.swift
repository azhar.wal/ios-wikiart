//
//  WAPaintingDismissalAnimator.swift
//  Cells
//
//  Created by Waleed Azhar on 10/3/17.
//  Copyright © 2017 Waleed Azhar. All rights reserved.
//

import UIKit

protocol WAPaintingAnimatorDataSource {
    func imageOfViewToAnimateOpen() -> UIImage
    func frameOfViewToAnimateOpen() -> CGRect
    func contentOffSetOfViewToAnimateOpen() -> CGPoint
    func willBeginToAnimateOpen()
    func willEndToAnimateOpen()
    
    func imageOfViewToAnimateClose() -> UIImage
    func frameOfViewToAnimateClose() -> CGRect
    func contentOffSetOfViewToAnimateClose() -> CGPoint
    func willBeginToAnimateClose()
    func willEndToAnimateClose()
}

class WAPaintingDismissalAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    
    private var animator: UIViewPropertyAnimator?
    
    public var fromDataSource: WAPaintingAnimatorDataSource
    public var toDataSource: WAPaintingAnimatorDataSource
    
    private let duration: TimeInterval = 0.4
    
    init(from: WAPaintingAnimatorDataSource, to: WAPaintingAnimatorDataSource) {
        self.fromDataSource = from
        self.toDataSource = to
        super.init()
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return duration
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let oldVC = transitionContext.viewController(forKey: .from)!
        let newVC = transitionContext.viewController(forKey: .to)!
        
        let container = transitionContext.containerView
      
        container.addSubview(newVC.view)
       
        let oldImage = fromDataSource.imageOfViewToAnimateClose()
        let imageView = UIImageView.init(image: oldImage)
        imageView.clipsToBounds = false
        container.addSubview(imageView)
       
        let oldFrame = fromDataSource.frameOfViewToAnimateClose()
        let oldOffSet = fromDataSource.contentOffSetOfViewToAnimateClose()
        imageView.frame = CGRect(origin:CGPoint(x:oldFrame.origin.x - oldOffSet.x, y:oldFrame.origin.y - oldOffSet.y), size: oldFrame.size)
        
        oldVC.view.removeFromSuperview()
       
        toDataSource.willBeginToAnimateClose()
        
        let offSet =  toDataSource.contentOffSetOfViewToAnimateClose()
        let finalRect = self.toDataSource.frameOfViewToAnimateClose()

        let animation:() -> Void =  {
            imageView.frame = CGRect(origin: CGPoint(x:0, y:finalRect.origin.y - offSet.y ), size: finalRect.size)
            
        }
        
        let completion:(Bool) -> Void = { b in
            self.toDataSource.willEndToAnimateClose()
            imageView.removeFromSuperview()
            transitionContext.completeTransition(b)

        }
        
        UIView.animate(withDuration: duration,
                       delay: 0.0,
                       usingSpringWithDamping: 0.6,
                       initialSpringVelocity: 1,
                       options: .curveEaseInOut,
                       animations: animation,
                       completion: completion)
        
    }
}
