//
//  WAPaintingAnimator.swift
//  Cells
//
//  Created by Waleed Azhar on 10/3/17.
//  Copyright © 2017 Waleed Azhar. All rights reserved.
//

import UIKit




class WAPaintingPresentationAnimator: NSObject, UIViewControllerAnimatedTransitioning  {
    
    private var animator: UIViewPropertyAnimator?
    
    public var oldDataSource: WAPaintingAnimatorDataSource
    public var newDataSource: WAPaintingAnimatorDataSource
    
    private let duration: TimeInterval = 0.4
    
    init(old: WAPaintingAnimatorDataSource, new:WAPaintingAnimatorDataSource) {
        self.oldDataSource = old
        self.newDataSource = new
        super.init()
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return duration
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {

        let newVC = transitionContext.viewController(forKey: .to)!

        let container = transitionContext.containerView
      
        let oldImage = oldDataSource.imageOfViewToAnimateOpen()
        let imageView = UIImageView.init(image: oldImage)
        container.addSubview(imageView)
        
        let oldFrame = oldDataSource.frameOfViewToAnimateOpen()
        let offSet =  oldDataSource.contentOffSetOfViewToAnimateOpen()
        imageView.frame = CGRect(origin:CGPoint(x:oldFrame.origin.x, y:oldFrame.origin.y - offSet.y), size: oldFrame.size)
      
        let newView = transitionContext.view(forKey: .to)
        let newFrame = transitionContext.finalFrame(for: newVC)
       
        oldDataSource.willBeginToAnimateOpen()
        
        let animation:() -> Void =  {
            let aspect = (oldImage.size)
            let size = CGSize(width: newFrame.height * (aspect.width/aspect.height), height:newFrame.height)
            let origin = CGPoint(x: (-(size.width/2)) + (newFrame.width/2), y: 0)
            imageView.frame = CGRect(origin:origin , size:size)
        }
        
        let completion:(Bool) -> Void = { b in
            imageView.removeFromSuperview()
            newView?.frame = newFrame
            container.addSubview(newView!)
            transitionContext.completeTransition(b)
            self.oldDataSource.willEndToAnimateOpen()
        }
        
        UIView.animate(withDuration: duration,
                       delay: 0.0,
                       usingSpringWithDamping: 0.6,
                       initialSpringVelocity: 1,
                       options: .curveEaseInOut,
                       animations: animation,
                       completion: completion)
        
    }
}
