//
//  WALodingResourceViewController.swift
//  WikiArt2.0
//
//  Created by Waleed Azhar on 2018-10-15.
//  Copyright © 2018 Waleed Azhar. All rights reserved.
//

import UIKit

final class WALodingResourceViewController<A:Codable>: UIViewController {
    
    private var build: (A?) -> UIViewController? = { _ in nil }
    
    init(resource:Resource<A>, build: @escaping (A?) -> UIViewController? ) {
        self.build = build
        super.init(nibName: nil, bundle: nil)
        // TODO: fix
        WebService().load(resource: resource) { [weak self] model in
            if let model = model, let vc = self?.build(model) {
                self?.addChild(vc)
                self?.view.addSubview(vc.view)
                vc.didMove(toParent: self)
            }
        }
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.prefersLargeTitles = false
        view.backgroundColor = .white
        activityIndicator.startAnimating()
    }
}
