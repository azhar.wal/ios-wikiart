//
//  WATextfieldViewController.swift
//  WikiArt2.0
//
//  Created by Waleed Azhar on 2018-10-15.
//  Copyright © 2018 Waleed Azhar. All rights reserved.
//

import UIKit

class WATextfieldViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textField: UITextView!
    
    private var heading: String?
    private var text: String?
    
    convenience init(heading: String?, biography: String? ) {
        self.init(nibName: nil , bundle: nil )
        
        self.heading = heading
        self.text = biography
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.titleLabel.text = heading
        self.textField.text = text
    }
    
}
