//
//  WACollectionViewController.swift
//  WikiArt2.0
//
//  Created by Waleed Azhar on 2017-09-24.
//  Copyright © 2017 Waleed Azhar. All rights reserved.
//

import UIKit

final class WAPaintingCollectionViewController: WALoadingIndicatorCollectionViewController, UICollectionViewDelegateFlexibleHeightLayout,  UIViewControllerTransitioningDelegate {

    var type: WAArtworksDataSourceType = .category

    var selectedCategory: WAArtworksCategory = .featured
    
    var selectedItem: ArtworksTypeServer?
    
    var artworksList: WAArtworksDataSource?
    
    var oldLastPaintingCount = 0
    
    var artistPage: WAArtistPage?
    
    convenience init( artist: WAArtistPage ) {
        
        self.init()

        self.artistPage = artist
        
        self.artworksList = WAArtworksDataSource(allPaintings: artist)
        
        type = .allPaintings
        
        self.artworksList!.completion = { [weak self] _ in
            self?.messageLabel.text = nil
            self?.hideLoading()
            self?.reloadData()
        }
        
        self.artworksList!.error = { [weak self] in
            self?.messageLabel.text = "An Error Occured"
            self?.hideLoading()
        }
    }
    
    
    override init () {
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.register( WAPaintingCollectionViewCell.self, forCellWithReuseIdentifier: WAPaintingCollectionViewCell.description() )
        let layout = WACollectionViewFlexibleHeightLayout()
        layout.delegate = self
        collectionView.setCollectionViewLayout(layout, animated: false)
        
        type == .category ? configureForCategories() : nil
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let list = artworksList, list.type == .favorites { loadPaintings() }
    }
    
    func configureForCategories() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Categories", style: .plain, target: self, action: #selector(pressed(_:)))
        navigationItem.backBarButtonItem?.title = ""
        
        loadPaintings()
    }
    
    func resetPage() {
        selectedCategory = .featured
        selectedItem = nil
        loadPaintings()
    }
    
    func loadPaintings() {

        toggleLoadingView()
        navigationItem.title = nil
        oldLastPaintingCount = 0
        artworksList = nil
        collectionView.reloadData()
        
        let completion: (Int) -> () = { [weak self] _ in
            self?.messageLabel.text = nil
            self?.hideLoading()
            self?.reloadData()
        }
        
        if selectedCategory == .favorites {
            artworksList = WAArtworksDataSource(favorites: .favorites, completion: completion)
            self.messageLabel.text = nil
            self.hideLoading()
            collectionView.reloadData()
            navigationItem.title = selectedCategory.title().capitalized
        }
        else {
            artworksList = WAArtworksDataSource( category: selectedCategory, categoryItem: selectedItem )
            artworksList?.completion = completion
        }
        
        artworksList!.error = { [weak self] in
            self?.messageLabel.text = "An Error Occured"
            self?.hideLoading()
        }
        
    }
 
    func reloadData() {
        
        if let item = selectedItem { navigationItem.title = item.title?.capitalized }
       
        else if type != .allPaintings {
            navigationItem.title = selectedCategory.title().capitalized
        }

        guard let paintings = artworksList?.paintingsList?.Paintings else { return }
 
        var indexPaths:[IndexPath] = []

        for i in oldLastPaintingCount..<paintings.count {
            indexPaths.append(IndexPath(row: i, section: 0))
        }

        collectionView.insertItems(at: indexPaths)
    }
    
    override func loadingIndicatorCollectionViewShouldShowLoadingIndicator() -> Bool {
        if artworksList?.canLoadMorePaintings ?? false {
            oldLastPaintingCount = artworksList!.paintingsList!.Paintings.count
            artworksList?.loadNextPage()
            return true
        }
        
        return false
    }
    
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        return WAMenuPresentationController(presentedViewController: presented, presenting: presenting)
    }
    
}

extension WAPaintingCollectionViewController: WAPaintingOptionsViewDelegate {
    
    func selected(artworkCategory: WAArtworksCategory, item: ArtworksTypeServer?) {
        selectedCategory = artworkCategory
        selectedItem = item
        presentedViewController?.dismiss(animated: true, completion: nil)
        loadPaintings()
    }
    
    @objc func pressed(_ sender: Any) {
        let vc = WAPaintingOptionsViewController(style: .plain)
        vc.delegate = self
      
        let nav = UINavigationController(root: vc)
        nav.modalPresentationStyle = .custom
        nav.transitioningDelegate = self

        present(nav, animated: true, completion: nil)
    }
    
}

extension WAPaintingCollectionViewController {

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return artworksList?.count ?? 0
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: WAPaintingCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: WAPaintingCollectionViewCell.description(), for: indexPath) as! WAPaintingCollectionViewCell
        if let painting = artworksList?.paintingsList?.Paintings[indexPath.row] { cell.configure(forPainting: painting) }
        
        return cell
    }

    func flexibleHeighCollectionViewLayoutAspectRatioForCellAt(indexPath: IndexPath) -> CGFloat {
        if let p = artworksList?.paintingsList?.Paintings[indexPath.row] {
            return p.height / p.width
        }
        
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let paintings = artworksList?.paintingsList else { return }
        let painting = paintings.Paintings[indexPath.row]
        let vc = WAArtworkDetailsViewController(artwork: painting)
        show(vc, sender: self)
    }
    
}

