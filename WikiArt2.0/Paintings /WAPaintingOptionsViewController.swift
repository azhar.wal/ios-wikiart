//
//  WAOptionsViewController.swift
//  WikiArt2.0
//
//  Created by Waleed Azhar on 2018-10-08.
//  Copyright © 2018 Waleed Azhar. All rights reserved.
//

import UIKit

protocol WAPaintingOptionsViewDelegate {
    func selected(artworkCategory: WAArtworksCategory, item: ArtworksTypeServer?)
}

final class WAPaintingOptionsViewController: UITableViewController {
    
    public var delegate: WAPaintingOptionsViewDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancel))
        
        title = "Categories"
        tableView.reloadData()
        tableView.register(WAMenuViewMenuItemTableViewCell.self, forCellReuseIdentifier: WAMenuViewMenuItemTableViewCell.description() )
    }
    
    @objc func cancel() {
        presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
}


extension WAPaintingOptionsViewController {
    
    public override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ArtworksCategoriesDataSource.artworksCategories.count
        
    }
    
    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: WAMenuViewMenuItemTableViewCell.description(), for: indexPath)
        let category = ArtworksCategoriesDataSource.artworksCategories[indexPath.row]
        cell.textLabel?.text = category.title()
        cell.accessoryType = ( ArtworksCategoriesDataSource.categoriesWithTypes.contains(category) ) ? .disclosureIndicator : .none
        return cell
    }
    
    public override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let category = ArtworksCategoriesDataSource.artworksCategories[indexPath.row]
        
        if ArtworksCategoriesDataSource.categoriesWithTypes.contains(category) {
                let tvc = WAOptionDetailTableViewController(artworkCategory: category )
                tvc.title = category.title()
                tvc.delegate = self
                tvc.tableView.tag = 2
                self.navigationController?.pushViewController(tvc, animated: true)

        } else { delegate?.selected(artworkCategory: category, item: nil) }

    }

}

extension WAPaintingOptionsViewController: WAOptionDetailTableViewDelegate {
    
    func selected(artworkCategory: WAArtworksCategory, item: ArtworksTypeServer) {
        delegate?.selected(artworkCategory: artworkCategory, item: item)
    }
    
    func selected(artistCategory: WAArtistsCategories, item: ArtistsDictionariesWithCategories) {
        
    }
    
}

