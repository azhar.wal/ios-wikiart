//
//  WAArtworkDetailsViewController.swift
//  WikiArt2.0
//
//  Created by Waleed Azhar on 2018-10-05.
//  Copyright © 2018 Waleed Azhar. All rights reserved.
//

import UIKit

final class WAArtworkDetailsViewController: UIViewController {

    @IBOutlet weak var artworkTitle: UILabel!
    
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var artistInfo: UIButton!
    
    @IBOutlet weak var favButton: WAFavoriteButton!
    
    @IBOutlet weak var collectionView: UICollectionView! {
        
        didSet {
            collectionView.dataSource = self
            collectionView.delegate = self
            collectionView.register(WAPaintingCollectionViewCell.self, forCellWithReuseIdentifier: WAPaintingCollectionViewCell.description())
        }
        
    }
    
    private let artwork: WAPainting
    
    private var relatedArt: WAPaintingsList! {
        
        didSet {
            collectionView.reloadData()
        }
        
    }
    
    private var scrollView: UIScrollView {
        return self.view as! UIScrollView
    }
    
    private var imageVC2: WAPhotoDetailViewController?

    init(artwork: WAPainting) {
        self.artwork = artwork
        super.init(nibName: nil, bundle: nil)
        
        Current.service.getRelatedPaintings(for: artwork) { [weak self] _relatedArt in
            if let _relatedArt = _relatedArt { self?.relatedArt = _relatedArt }
        }
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @IBAction func artistNamePresses(_ sender: Any) {
    
        let bioUrl: WAUrl = .parts(Current.language, WAUrlLocation.artist(artwork.artistEndPoint), WAUrlParameters.singleValue(.jsonV2))
        
        let resource = Resource<WAArtistPage>.init(url: bioUrl)

        let loadingVC = WALodingResourceViewController(resource: resource) { artistPage in
            let vc = UIStoryboard.init(name: "WAArtistDetails", bundle: nil).instantiateInitialViewController() as! WAArtistDetailsTableViewController
            vc.artistPage = artistPage
            return vc
            
        }
        
        navigationController?.pushViewController(loadingVC, animated: true)
    }
    
    @IBAction func didTapArtwork(_ sender: UITapGestureRecognizer) {

        UIView.animate(withDuration: 0.1, delay: 0, options: .curveEaseOut, animations: {
            self.scrollView.contentOffset = .init(x: 0, y: -self.scrollView.safeAreaInsets.top)
        }) { bool in
            guard let image = self.imageView.image else { return }
            
            let imageVC = WAPhotoDetailViewController(image: image )
            imageVC.transitioningDelegate = self
            self.definesPresentationContext = true
            self.imageVC2 = imageVC
            self.present(imageVC, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func favoritePressed(_ sender: Any) {
        let favs = Current.favorites
        let contains = favs.contains(artwork)
        contains ? favs.remove(painting: artwork) : favs.append(painting: artwork)
        favButton.configure(faved: !contains)
    }
    
    
}

// MARK: Life Cycle Methods
extension WAArtworkDetailsViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.prefersLargeTitles = false
        
        imageView.loadPainting(painting: artwork, size: .original)
        imageView.heightAnchor.constraint(equalTo: view.widthAnchor, multiplier: artwork.aspectRatio).isActive = true
        
        artworkTitle.text = artwork.titled
        artistInfo.setTitle(artwork.artistInfo, for: .normal)
        
        let favs = Current.favorites
        let contains = favs.contains(artwork)
        favButton.configure(faved: contains)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        scrollView.isScrollEnabled = true
        scrollView.contentSize = CGSize(width: 0, height: collectionView!.frame.maxY + 8 )
    }
    
}

extension WAArtworkDetailsViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let painting = relatedArt.Paintings[indexPath.row]
        show(WAArtworkDetailsViewController(artwork: painting), sender: self)
        
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height: CGFloat = 240
        let maxWidth: CGFloat = collectionView.frame.width
        
        if let painting = relatedArt?.Paintings[indexPath.row] {
            return CGSize(width:min(height*painting.width/painting.height, maxWidth), height: WAPaintingCollectionViewCell.UnderHangHeight + height)
        }
        return CGSize(width: height, height: maxWidth)
    }
    
}

extension WAArtworkDetailsViewController: UICollectionViewDataSource {
   
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return relatedArt?.Paintings.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: WAPaintingCollectionViewCell.description(), for: indexPath) as! WAPaintingCollectionViewCell
        
        if let painting = relatedArt?.Paintings[indexPath.row] {
            cell.configure(forPainting: painting, paintingSize: .portrait)
        }
        
        return cell
    }
    
    
}

extension WAArtworkDetailsViewController: UIViewControllerTransitioningDelegate, WAPaintingAnimatorDataSource {
    
    func imageOfViewToAnimateOpen() -> UIImage {
        return self.imageView.image!
    }
    
    func frameOfViewToAnimateOpen() -> CGRect {
        return view.convert(self.imageView.frame, to: UIApplication.shared.keyWindow?.rootViewController?.view)
    }
    
    func contentOffSetOfViewToAnimateOpen() -> CGPoint {
        return CGPoint(x: 0, y: 0)
    }
    
    func willBeginToAnimateOpen() {
        imageView.isHidden = true
    }
    
    func willEndToAnimateOpen() {
        imageView.isHidden = false
    }
    
    func imageOfViewToAnimateClose() -> UIImage {
        return self.imageView.image!
    }
    
    func frameOfViewToAnimateClose() -> CGRect {
        return view.convert(self.imageView.frame, to: UIApplication.shared.keyWindow?.rootViewController?.view)
    }
    
    func contentOffSetOfViewToAnimateClose() -> CGPoint {
        return CGPoint(x: 0, y: 0)
    }
    
    func willBeginToAnimateClose() {
        imageView.isHidden = true
    }
    
    func willEndToAnimateClose() {
        imageView.isHidden = false
    }
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        guard let ds = imageVC2 else {return nil}
        let animator = WAPaintingPresentationAnimator(old: self, new: ds)
        return animator
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        var animator:UIViewControllerAnimatedTransitioning?
        
        if let ds = imageVC2 {
            animator = WAPaintingDismissalAnimator( from: ds, to: self )
        }
        
        return animator
    }
}
