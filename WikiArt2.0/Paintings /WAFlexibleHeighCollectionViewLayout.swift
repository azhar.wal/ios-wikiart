//
//  WAFlexibleHeighCollectionViewLayout.swift
//  WikiArt2.0
//
//  Created by Waleed Azhar on 1017-09-24.
//  Copyright © 1017 Waleed Azhar. All rights reserved.
//

import UIKit

public protocol UICollectionViewDelegateFlexibleHeightLayout : UICollectionViewDelegate {
    func flexibleHeighCollectionViewLayoutAspectRatioForCellAt(indexPath: IndexPath) -> CGFloat
}

class WACollectionViewFlexibleHeightLayout: UICollectionViewLayout {
    
    open weak var delegate: UICollectionViewDelegateFlexibleHeightLayout?
    open var maximumItemsPerRow = 2
    open var minimumLineSpacing: CGFloat = 10
    open var minimumInteritemSpacing: CGFloat = 10
    open var estimatedItemSize: CGSize = CGSize.zero
    open var footerHeight: CGFloat = WAPaintingCollectionViewCell.UnderHangHeight
    open var maxY: CGFloat = 0.0
    private var oldWidth:CGFloat = -1.0
    private var lastAttribute: UICollectionViewLayoutAttributes?
    private var attributes:[UICollectionViewLayoutAttributes] = []
    private var size: CGSize?
    
    private var numberOfItems: Int {
        return self.collectionView?.dataSource?.collectionView(self.collectionView!, numberOfItemsInSection: 0) ?? 0
    }
    
    override var collectionViewContentSize: CGSize {
        if let s = size {
            return s
        }
        guard let cv = self.collectionView else {fatalError("no cv")}
        if attributes == [] {
            return .zero
        }
        var maxY: CGFloat = 0.0;
        
        let atts = attributes.dropFirst(attributes.count - (maximumItemsPerRow * 3) < 0 ? 0 : attributes.count - (maximumItemsPerRow * 3))
        let max = atts.max { (a, b) -> Bool in
            return a.frame.maxY <= b.frame.maxY
        }
        
        maxY = max?.frame.maxY ?? 0
        self.maxY = maxY
        
        size = CGSize(width: cv.frame.width, height: maxY + 32.0)
        return size!
    }
    
    override func prepare() {
        if numberOfItems <= attributes.count {
            return
        }
        attributes = []
        guard let cv = self.collectionView else {fatalError("no cv")}
        guard let dataSource = cv.dataSource else {fatalError("no data source")}
        
        let x0: CGFloat = 10
        let y0: CGFloat = 8
        
        let interItemSpacingMagnitude = minimumInteritemSpacing * (CGFloat(maximumItemsPerRow) - 1.0)
        let avilableWidth = cv.frame.width - interItemSpacingMagnitude - (20)
        let itemWidth = avilableWidth / (CGFloat(maximumItemsPerRow))
        
        var xS = [x0]
        
        if dataSource.collectionView(cv, numberOfItemsInSection: 1) > 1 {
            for _ in (1...maximumItemsPerRow) {
                xS.append(xS.last! + itemWidth + minimumInteritemSpacing)
            }
        }
        
        var maxYS: [CGFloat: UICollectionViewLayoutAttributes] = [:]
        
        for i in 0..<min(maximumItemsPerRow,dataSource.collectionView(cv, numberOfItemsInSection: 1))  {
            let indexPath = IndexPath(item: i, section: 0)
            let att = UICollectionViewLayoutAttributes(forCellWith: indexPath)
            
            let aspectRatio = delegate?.flexibleHeighCollectionViewLayoutAspectRatioForCellAt(indexPath: indexPath) ?? 1
            
            att.frame = CGRect(x: xS[i % maximumItemsPerRow],
                               y: y0,
                               width: itemWidth,
                               height: (itemWidth * aspectRatio) + footerHeight)
            
            maxYS[xS[i % maximumItemsPerRow]] = att
            attributes.append(att)
        }
        
        guard dataSource.collectionView(cv, numberOfItemsInSection: 1) > maximumItemsPerRow else {
            lastAttribute = attributes.last
            return
        }
        
        for i in maximumItemsPerRow..<dataSource.collectionView(cv, numberOfItemsInSection: 1) {
            
            let max = maxYS.min(by: { (arg1, arg2) -> Bool in
                let (_, a) = arg2
                let (_, b) = arg1
                return a.frame.maxY > b.frame.maxY
            })!
            
            let indexPath = IndexPath(item: i, section: 0)
            let att = UICollectionViewLayoutAttributes(forCellWith: indexPath)
            
            let aspectRatio = delegate?.flexibleHeighCollectionViewLayoutAspectRatioForCellAt(indexPath: indexPath) ?? 1
            
            att.frame = CGRect(x: max.value.frame.minX,
                               y: max.value.frame.maxY + minimumLineSpacing,
                               width: itemWidth,
                               height: (itemWidth * aspectRatio) + footerHeight)
            maxYS[max.value.frame.minX] = att
            
            attributes.append(att)
        }
        lastAttribute = attributes.last
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        return attributes
    }
    
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return attributes[indexPath.row]
    }
    
    override func invalidateLayout() {
        self.attributes = []
        size = nil
    }
    
    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return false
    }
}

extension UIEdgeInsets {
    
    var horizontalInsetsMagnitude: CGFloat {
        return self.left + self.right
    }
    
}
