//
//  WAPaintingCollectionViewCell.swift
//  WikiArt2.0
//
//  Created by Waleed Azhar on 2017-09-24.
//  Copyright © 2017 Waleed Azhar. All rights reserved.
//

import UIKit

class WAPaintingCollectionViewCell: UICollectionViewCell {
    
    static var UnderHangHeight: CGFloat = 30
    
    let paintingView = UIImageView()
    let paintingNameLabel = UILabel()
    let artistNameLabel = UILabel()
    let stackView = UIStackView()
    
    private var isFirst = true
    
    var paintingHeight = NSLayoutConstraint()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        contentView.addSubview(paintingView)
        self.clipsToBounds = true
        paintingView.clipsToBounds = true
        paintingView.contentMode = .scaleAspectFill
        
        self.translatesAutoresizingMaskIntoConstraints = false
        contentView.translatesAutoresizingMaskIntoConstraints = false
        paintingView.translatesAutoresizingMaskIntoConstraints = false
        stackView.translatesAutoresizingMaskIntoConstraints = false
        paintingNameLabel.translatesAutoresizingMaskIntoConstraints = false
        artistNameLabel.translatesAutoresizingMaskIntoConstraints = false
        
        stackView.axis = .vertical
        stackView.distribution = .fillProportionally
        stackView.alignment = .fill
        stackView.spacing = 0.0
        stackView.addArrangedSubview(paintingNameLabel)
        stackView.addArrangedSubview(artistNameLabel)
        isOpaque = true
        
        contentView.addSubview(stackView)
        
        paintingNameLabel.font = UIFont.systemFont(ofSize: 12, weight: .bold)
        paintingNameLabel.textColor = .black
        paintingNameLabel.isOpaque = true
        artistNameLabel.font = UIFont.systemFont(ofSize: 12, weight: .light)
        artistNameLabel.textColor = .darkGray
        artistNameLabel.lineBreakMode = .byTruncatingMiddle
        artistNameLabel.isOpaque = true
        
        self.pinToEdges(view: stackView, excludingEdge: .Top)
        stackView.heightAnchor.constraint(equalToConstant: WAPaintingCollectionViewCell.UnderHangHeight).isActive = true
        self.pinToEdges(view: paintingView, excludingEdge: .Bottom)
        paintingView.bottomAnchor.constraint(equalTo: stackView.topAnchor, constant: -4).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    override var isSelected: Bool {
        didSet{
            renderCellSelection(selected: isSelected)
        }
    }
    override var isHighlighted: Bool {
        didSet{
           renderCellSelection(selected: isHighlighted)
        }
    }
    
    func renderCellSelection(selected: Bool) {
        
        let selectedAlpha: CGFloat = 0.8
        let normalAlpha: CGFloat = 1

        self.paintingView.alpha = selected ? selectedAlpha : normalAlpha
        self.paintingNameLabel.alpha = selected ? selectedAlpha : normalAlpha
        self.artistNameLabel.alpha = selected ? selectedAlpha  : normalAlpha

    }

    override func prepareForReuse() {
        super.prepareForReuse()
        paintingView.image = nil
    }
    
    func configure(forPainting painting: WAPainting, paintingSize: WAPaintingSize = .medium, textAlignment: NSTextAlignment = .left ) {
        paintingView.loadPainting(painting: painting, size: paintingSize)
        paintingNameLabel.text = painting.titled
        paintingNameLabel.textAlignment = textAlignment
        artistNameLabel.text = painting.artistNamed
        artistNameLabel.textAlignment = textAlignment
    }
    
}
