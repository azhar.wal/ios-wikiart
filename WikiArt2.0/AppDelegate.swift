//
//  AppDelegate.swift
//  WikiArt2.0
//
//  Created by Waleed Azhar on 2017-09-24.
//  Copyright © 2017 Waleed Azhar. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics

// Cache for images
var imageCache = NSCache<NSURL, UIImage>()
var Current: WikiArtApp = WikiArtApp(languageCode: Locale.autoupdatingCurrent.languageCode)

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow? = UIWindow()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        Fabric.with([Crashlytics.self])
        window?.rootViewController = UIViewController.appRootViewController()
        window?.makeKeyAndVisible()
        
        return true
    }
    
    func applicationDidReceiveMemoryWarning(_ application: UIApplication) {
        imageCache = NSCache<NSURL, UIImage>()
    }
    
}

